1.2 (2016-02-08)
===================

- A bug in using cartesian basis functions was identified and fixed. 

1.1 (2016-01-13)
===================

- The "make install" was fixed.

1.0 (2015-12-22)
===================

See the release notes at http://daltonprogram.org for a list of new features in
Dalton and LSDalton.

