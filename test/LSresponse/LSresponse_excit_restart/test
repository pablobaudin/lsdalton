#!/usr/bin/env python

import os
import sys
import shutil

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from runtest_lsdalton import Filter, TestRun

test = TestRun(__file__, sys.argv)

# Input files:
DAL1 = "LSresponse_excit"
DAL2 = "LSresponse_excit_restart"
MOL  = "CO"

# Define filter for restarted calculation
# =======================================
f = Filter()
f.add(string = 'Final HF energy:',
      abs_tolerance = 1.0e-7)

f.add(from_string = 'The following excitation energies are used',
      num_lines = 6,
      abs_tolerance = 1.0e-6)

f.add(string = 'Restart from')
f.add(string = 'Allocated memory (TOTAL)')

# Build list of files to save for restart
# =======================================
filename = 'scf_rsp_eigenvecs_ao.restart'
get_restart = '-get " ' + filename + ' "'
put_restart = '-put " ' + filename + ' "'


# Run first calculation
# =====================
test.run([DAL1+'.dal'], [MOL+'.mol'], args = get_restart)

name = "_".join([DAL1,MOL]) 

# Remove prefix from restart file
# ===============================
tmp = ".".join([name,filename])
shutil.move(tmp,filename)


# Run second calculation with RESTART keyword
# ===========================================
test.run([DAL2+'.dal'], [MOL+'.mol'], {'out': f}, args = put_restart)


# Delete file
# ===========
os.remove(filename)


sys.exit(test.return_code)

