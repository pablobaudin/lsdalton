#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from runtest_lsdalton import Filter, TestRun

test = TestRun(__file__, sys.argv)

f = Filter()
f.add(string = 'Hartree-Fock energy',
      rel_tolerance = 1.0e-8)

f.add(string = 'Correlation energy',
      rel_tolerance = 5.0e-4)

f.add(string = 'DEC-RI-MP2 GRADIENT NORM',
      rel_tolerance = 5.0e-4)
f.add(string = 'DEC-RI-MP2 GRADIENT RMS',
      rel_tolerance = 5.0e-4)
f.add(string = 'Total RIMP2 energy',
      rel_tolerance = 5.0e-4)

f.add(from_string = 'contribution to gradient',
      num_lines   = 9,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'Nuclear repulsion gradient',
      num_lines   = 9,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'One-electron integral gradient',
      num_lines   = 9,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'Reorthonormalization gradient',
      num_lines   = 9,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'Fock matrix derivative contribution',
      num_lines   = 9,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'Ltheta contribution',
      num_lines   = 9,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'TOTAL RI-MP2 MOLECULAR GRADIENT',
      num_lines   = 9,
      abs_tolerance = 5.0e-4)

f.add(string = 'Allocated memory (TOTAL)')

f.add(string = 'Memory in use for array4',
      rel_tolerance = 1.0e-9)

test.run(['decrimp2_gradient.dal'], ['LiH3.mol'], {'out': f})

sys.exit(test.return_code)
