 THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDALTON - An electronic structure program  **********    
     ******************************************************************    
  
  
      This is output from LSDALTON 2016.alpha
  
  
      IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
      THE FOLLOWING PAPER SHOULD BE CITED:
      
      K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
      L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
      P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
      J. J. Eriksen, P. Ettenhuber, B. Fernandez,
      L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
      A. Halkier, C. Haettig, H. Heiberg,
      T. Helgaker, A. C. Hennum, H. Hettema,
      E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
      M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
      D. Jonsson, P. Joergensen, J. Kauczor,
      S. Kirpekar, T. Kjaergaard, W. Klopper,
      S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
      A. Krapp, K. Kristensen, A. Ligabue,
      O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
      C. Neiss, C. B. Nielsen, P. Norman,
      J. Olsen, J. M. H. Olsen, A. Osted,
      M. J. Packer, F. Pawlowski, T. B. Pedersen,
      P. F. Provasi, S. Reine, Z. Rinkevicius,
      T. A. Ruden, K. Ruud, V. Rybkin,
      P. Salek, C. C. M. Samson, A. Sanchez de Meras,
      T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
      K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
      P. R. Taylor, A. M. Teale, E. I. Tellgren,
      D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
      O. Vahtras, M. A. Watson, D. J. D. Wilson,
      M. Ziolkowski, and H. AAgren,
      "The Dalton quantum chemistry program system",
      WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                                
      LSDALTON authors in alphabetical order (main contribution(s) in parenthesis)
      ----------------------------------------------------------------------------
      Vebjoern Bakken,         University of Oslo,        Norway    (Geometry optimizer)
      Radovan Bast,            KTH Stockholm,             Sweden    (CMake, Testing)
      Pablo Baudin,            Aarhus University,         Denmark   (DEC,CCSD)
      Sonia Coriani,           University of Trieste,     Italy     (Response)
      Patrick Ettenhuber,      Aarhus University,         Denmark   (CCSD)
      Janus Juul Eriksen,      Aarhus University,         Denmark   (CCSD(T), DEC)
      Trygve Helgaker,         University of Oslo,        Norway    (Supervision)
      Stinne Hoest,            Aarhus University,         Denmark   (SCF optimization)
      Ida-Marie Hoeyvik,       Aarhus University,         Denmark   (Orbital localization, SCF optimization)
      Robert Izsak,            University of Oslo,        Norway    (ADMM)
      Branislav Jansik,        Aarhus University,         Denmark   (Trilevel, orbital localization)
      Poul Joergensen,         Aarhus University,         Denmark   (Supervision)
      Joanna Kauczor,          Aarhus University,         Denmark   (Response solver)
      Thomas Kjaergaard,       Aarhus University,         Denmark   (Response, Integrals, DEC, SCF, Readin, MPI, Scalapack)
      Andreas Krapp,           University of Oslo,        Norway    (FMM, dispersion-corrected DFT)
      Kasper Kristensen,       Aarhus University,         Denmark   (Response, DEC)
      Patrick Merlot,          University of Oslo,        Norway    (ADMM)
      Cecilie Nygaard,         Aarhus University,         Denmark   (SOEO)
      Jeppe Olsen,             Aarhus University,         Denmark   (Supervision)
      Simen Reine,             University of Oslo,        Norway    (Integrals, geometry optimizer)
      Vladimir Rybkin,         University of Oslo,        Norway    (Geometry optimizer, dynamics)
      Pawel Salek,             KTH Stockholm,             Sweden    (FMM, DFT functionals)
      Andrew M. Teale,         University of Nottingham   England   (E-coefficients)
      Erik Tellgren,           University of Oslo,        Norway    (Density fitting, E-coefficients)
      Andreas J. Thorvaldsen,  University of Tromsoe,     Norway    (Response)
      Lea Thoegersen,          Aarhus University,         Denmark   (SCF optimization)
      Mark Watson,             University of Oslo,        Norway    (FMM)
      Marcin Ziolkowski,       Aarhus University,         Denmark   (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
 Who compiled             | pablo
 Host                     | pablo-AU
 System                   | Linux-3.13.0-37-generic
 CMake generator          | Unix Makefiles
 Processor                | x86_64
 64-bit integers          | OFF
 MPI                      | OFF
 Fortran compiler         | /usr/bin/gfortran
 Fortran compiler version | GNU Fortran (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4
 C compiler               | /usr/bin/gcc
 C compiler version       | gcc (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4
 C++ compiler             | /usr/bin/g++
 C++ compiler version     | g++ (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4
 BLAS                     | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                          | libmkl_gf_lp64.so;/opt/intel/composer_xe_2015.2.16
                          | 4/mkl/lib/intel64/libmkl_sequential.so;/opt/intel/
                          | composer_xe_2015.2.164/mkl/lib/intel64/libmkl_core
                          | .so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/l
                          | ib/x86_64-linux-gnu/libm.so
 LAPACK                   | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                          | libmkl_lapack95_lp64.a;/opt/intel/composer_xe_2015
                          | .2.164/mkl/lib/intel64/libmkl_gf_lp64.so
 Static linking           | OFF
 Last Git revision        | d3851c5f9cd79119b5c699428dccacf686835bf0
 Git branch               | pablo/ricc2
 Configuration time       | 2015-12-14 18:23:15.864411

Start simulation
     Date and time (Linux)  : Mon Dec 14 18:23:31 2015
     Host name              : pablo-AU                                
                      
 -----------------------------------------
          PRINTING THE MOLECULE.INP FILE 
 -----------------------------------------
                      
  BASIS                                   
  6-31G*                                  
                                          
                                          
  Atomtypes=1 Angstrom                                        
  Charge=1.0 Atoms=2                                          
  H         0.00000       0.00000         0.00000             
  H         0.7375        0.00000         0.00000             
                      
 -----------------------------------------
          PRINTING THE LSDALTON.INP FILE 
 -----------------------------------------
                      
  **WAVE FUNCTIONS                                                                                    
  .HF                                                                                                 
  *DENSOPT                                                                                            
  .ARH                                                                                                
  .START                                                                                              
  H1DIAG                                                                                              
  .CONVTHR                                                                                            
  1.0d-8                                                                                              
  **GENERAL                                                                                           
  .NOGCBASIS                                                                                          
  **CC                                                                                                
  .MP2                                                                                                
  .CANONICAL                                                                                          
  .MEMORY                                                                                             
  2                                                                                                   
  *CCRESPONSE                                                                                         
  .NEXCIT                                                                                             
  1                                                                                                   
  .P_EOM_MBPT2                                                                                        
  .THRES                                                                                              
  1.0e-7                                                                                              
  *END OF INPUT                                                                                       
  
  MOLPRINT IS SET TO       0
  Coordinates are entered in Angstroms and converted to atomic units.
  Conversion factor : 1 bohr = 0.52917721 A


                    Cartesian Coordinates Linsca (au)
                    ---------------------------------

   Total number of coordinates:  6
   Written in atomic units    
 
   1   H        x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000
 
   4   H        x      1.3936730238
   5            y      0.0000000000
   6            z      0.0000000000
 
 
 BASISSETLIBRARY  : REGULAR  
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  6-31G*                                            
  CHARGES:    1.0000
                      
   
  CALLING BUILD BASIS WITH DEFAULT REGULAR   BASIS
   
 OPENING FILE/home/pablo/Work/lsdalton_ricc2/build_dbg/basis/6-31G*
 PRINT MOLECULE AND BASIS IN FULL INPUT
                      
THE MOLECULE
 --------------------------------------------------------------------
 Molecular Charge                    :    0.0000
    Regular basisfunctions             :        4
    Valence basisfunctions             :        0
    Primitive Regular basisfunctions   :        8
    Primitive Valence basisfunctions   :        0
 --------------------------------------------------------------------
                      
                      
 --------------------------------------------------------------------
  atom  charge  Atomicbasis    Phantom   nPrimREG  nContREG
 --------------------------------------------------------------------
     1   1.000  6-31G*          F              4           2
     2   1.000  6-31G*          F              4           2
                      
 The cartesian centers in Atomic units. 
  ATOM  NAME  ISOTOPE            X                 Y                 Z     
     1  H           1        0.00000000        0.00000000        0.00000000
     2  H           1        1.39367302        0.00000000        0.00000000
                      
                      
Atoms and basis sets
  Total number of atoms        :      2
  THE  REGULAR   is on R =   1
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 H      1.000 6-31G*                     4        2 [4s|2s]                                      
      2 H      1.000 6-31G*                     4        2 [4s|2s]                                      
---------------------------------------------------------------------
total          2                                8        4
---------------------------------------------------------------------
                      
 Configuration:
 ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

   You have requested Augmented Roothaan-Hall optimization
   => explicit averaging is turned off!

Expand trust radius if ratio is larger than:            0.75
Contract trust radius if ratio is smaller than:         0.25
On expansion, trust radius is expanded by a factor      1.20
On contraction, trust radius is contracted by a factor  0.70

 H1DIAG does not work well with only few saved microvectors for ARH...
 Resetting max. size of subspace in ARH linear equations to:           5

Density subspace min. method    : None                    
Density optimization : Augmented RH optimization          

 Maximum size of Fock/density queue in averaging:          10

Convergence threshold for gradient:   0.10E-07
We perform the calculation in the standard input basis
 
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
 The SCF Convergence Criteria is applied to the gradnorm in OAO basis
 We activate the Atomic Fragment Optimization Correction (AFOC)

 End of configuration!


 Optimize first density for H1 operator

 Preparing to do S^1/2 decomposition...
Matrix S     has nnz=        16 sparsity:    100.000 %
Matrix AO D  has nnz=        16 sparsity:    100.000 %
  Relative convergence threshold for solver:   1.0000000000000000E-002
  SCF Convergence criteria for gradient norm:   1.0000000000000000E-008
** Get Fock matrix number   1
 The Coulomb energy contribution    1.5108131303146997     
 The Exchange energy contribution   0.75540656515734994     
 The Fock energy contribution  -0.75540656515734983     
**************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift    OAO gradient     ###
******************************************************************  ********************###
  1     -1.0739762891    0.00000000000    0.00      0.00000    0.00    2.41E-01  ###
***********************************************************************************************%%%
 Trust Radius   Max element     Norm     RHshift       Ratio  Dpar/Dtot  Ndens(FIFO)    SCF it %%%
***********************************************************************************************%%%
** Make average of the last F and D matrices
Matrix AO D  has nnz=        16 sparsity:    100.000 %
Matrix AO F  has nnz=        16 sparsity:    100.000 %
Matrix OAO D has nnz=        16 sparsity:    100.000 %
Matrix OAO F has nnz=        16 sparsity:    100.000 %
** Get new density 
 OAO gradnorm  0.24090161827297757     
 Number of densities in queue:           0
CROP scheme, truncated subspace (CORE) - max. no. of vectors is  5

    E(LUMO):                      0.272831 au
   -E(HOMO):                     -0.518049 au
   -------------------------------------
    HOMO-LUMO Gap (by diag.):     0.790880 au

 First mu:   0.0000000000000000     
  Error of red space iteration  1:        0.0108265354 (mu =       0.00, xmax =   0.687303E-01, xnorm =   0.194399E+00, THR =   0.240902E-02)
  Error of red space iteration  2:        0.0000000000 (mu =       0.00, xmax =   0.657743E-01, xnorm =   0.186038E+00, THR =   0.240902E-02)
Newton equations converged in  2 iterations!
No. of matmuls in get_density:    39
 >>>  CPU Time used in SCF iteration is   0.01 seconds
 >>> wall Time used in SCF iteration is   0.01 seconds
** Get Fock matrix number   2
 The Coulomb energy contribution    1.3302703981081807     
 The Exchange energy contribution   0.66513519905409035     
 The Fock energy contribution  -0.66513519905409024     
 0.60000 (xnorm)    0.06577    0.18604   -0.00        1.1560    0.0000         0          1    %%%
 Trust radius based on max norm!
 Large ratio, double trust-radius
 Absolute max step allowed is  0.59999999999999998       , resetting trust radius!
  2     -1.1257863535   -0.05181006447    0.00      0.00000   -0.00    3.41E-02  ###
** Make average of the last F and D matrices
Matrix AO D  has nnz=        16 sparsity:    100.000 %
Matrix AO F  has nnz=        16 sparsity:    100.000 %
Matrix OAO D has nnz=        16 sparsity:    100.000 %
Matrix OAO F has nnz=        16 sparsity:    100.000 %
** Get new density 
 OAO gradnorm   3.4089112487228700E-002
 Number of densities in queue:           1
CROP scheme, truncated subspace (CORE) - max. no. of vectors is  5

    E(LUMO):                      0.244042 au
   -E(HOMO):                     -0.589090 au
   -------------------------------------
    HOMO-LUMO Gap (by diag.):     0.833132 au

 First mu:   0.0000000000000000     
  Error of red space iteration  1:        0.0012571851 (mu =       0.00, xmax =   0.108172E-01, xnorm =   0.305957E-01, THR =   0.340891E-03)
  Error of red space iteration  2:        0.0000000000 (mu =       0.00, xmax =   0.104325E-01, xnorm =   0.295075E-01, THR =   0.340891E-03)
Newton equations converged in  2 iterations!
No. of matmuls in get_density:    43
 >>>  CPU Time used in SCF iteration is   0.01 seconds
 >>> wall Time used in SCF iteration is   0.01 seconds
** Get Fock matrix number   3
 The Coulomb energy contribution    1.3021372400485123     
 The Exchange energy contribution   0.65106862002425625     
 The Fock energy contribution  -0.65106862002425603     
 0.60000 (xnorm)    0.01043    0.02951   -0.00        0.9945    0.9884         1          2    %%%
 Trust radius based on max norm!
 Large ratio, double trust-radius
 Absolute max step allowed is  0.59999999999999998       , resetting trust radius!
  3     -1.1267866685   -0.00100031501    0.00      0.00000   -0.00    2.25E-04  ###
** Make average of the last F and D matrices
Matrix AO D  has nnz=        16 sparsity:    100.000 %
Matrix AO F  has nnz=        16 sparsity:    100.000 %
Matrix OAO D has nnz=        16 sparsity:    100.000 %
Matrix OAO F has nnz=        16 sparsity:    100.000 %
** Get new density 
 OAO gradnorm   2.2543908018506735E-004
 Number of densities in queue:           2
CROP scheme, truncated subspace (CORE) - max. no. of vectors is  5

    E(LUMO):                      0.239109 au
   -E(HOMO):                     -0.596623 au
   -------------------------------------
    HOMO-LUMO Gap (by diag.):     0.835732 au

 First mu:   0.0000000000000000     
  Error of red space iteration  1:        0.0000085059 (mu =       0.00, xmax =   0.709125E-04, xnorm =   0.200571E-03, THR =   0.225439E-06)
  Error of red space iteration  2:        0.0000000000 (mu =       0.00, xmax =   0.683342E-04, xnorm =   0.193278E-03, THR =   0.225439E-06)
Newton equations converged in  2 iterations!
No. of matmuls in get_density:    41
 >>>  CPU Time used in SCF iteration is   0.01 seconds
 >>> wall Time used in SCF iteration is   0.01 seconds
** Get Fock matrix number   4
 The Coulomb energy contribution    1.3023204579986860     
 The Exchange energy contribution   0.65116022899934312     
 The Fock energy contribution  -0.65116022899934300     
 0.60000 (xnorm)    0.00007    0.00019   -0.00        1.0000    1.0000         2          3    %%%
 Trust radius based on max norm!
 Large ratio, double trust-radius
 Absolute max step allowed is  0.59999999999999998       , resetting trust radius!
  4     -1.1267867121   -0.00000004357    0.00      0.00000   -0.00    3.78E-09  ###
SCF converged in      4 iterations
 >>>  CPU Time used in **ITER is   0.02 seconds
 >>> wall Time used in **ITER is   0.02 seconds

Total no. of matmuls in SCF optimization:        188

 Number of occupied orbitals:           1
 Number of virtual orbitals:           3

 Number of occupied orbital energies to be found:           1
 Number of virtual orbital energies to be found:           1


Calculation of HOMO-LUMO gap
============================

Calculation of occupied orbital energies converged in     1 iterations!

Calculation of virtual orbital energies converged in     3 iterations!

    E(LUMO):                         0.239141 au
   -E(HOMO):                        -0.596577 au
   ------------------------------
    HOMO-LUMO Gap (iteratively):     0.835719 au


********************************************************
 it       dE(HF)          exit   RHshift    RHinfo 
********************************************************
  1    0.00000000000    0.0000    0.0000    0.0000000
  2   -0.05181006447    0.0000   -0.0000    0.0000000
  3   -0.00100031501    0.0000   -0.0000    0.0000000
  4   -0.00000004357    0.0000   -0.0000    0.0000000

======================================================================
                   LINSCF ITERATIONS:
  It.nr.               Energy                 OAO Gradient norm
======================================================================
    1            -1.07397628905342457095      0.240901618272978D+00
    2            -1.12578635352435751926      0.340891124872287D-01
    3            -1.12678666853814402060      0.225439080185067D-03
    4            -1.12678671211113545780      0.377574963062968D-08

      SCF converged !!!! 
         >>> Final results from LSDALTON <<<


      Final HF energy:                        -1.126786712111
      Nuclear repulsion:                       0.717528418034
      Electronic energy:                      -1.844315130145



-- Full molecular info --

FULL: Overall charge of molecule :      0

FULL: Number of electrons        :      2
FULL: Number of atoms            :      2
FULL: Number of basis func.      :      4
FULL: Number of aux. basis func. :      0
FULL: Number of core orbitals    :      0
FULL: Number of valence orbitals :      1
FULL: Number of occ. orbitals    :      1
FULL: Number of virt. orbitals   :      3
FULL: Local memory use type full :  0.31E-06
FULL: Distribute matrices        : F

 Orbital energies:
           1 -0.59657745057284584     
           2  0.23914111383809233     
           3  0.77351770964802380     
           4   1.4077264605422239     
 Calculating carmom matrix for DEC calculation...
 Memory set in input to be:    2.000     GB
 Using canonical orbitals as requested in input!



Full molecular calculation is carried out...


 ================================================ 
              Full molecular driver               
 ================================================ 


--------------------------
  Coupled-cluster energy  
--------------------------

Wave function    = MP2     
MaxIter          =  100
Num. b.f.        =    4
Num. occ. orb.   =    1
Num. unocc. orb. =    3
Convergence      =  0.1E-08
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    F

 ### Starting CC iterations
 ### ----------------------
 ###  Iteration     Residual norm          CC energy
 ###      1         0.239038650E-15       -0.173541393E-01
 CCSOL: MAIN LOOP      : 0.161    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Hooray! CC equation is solved!
CCSOL: Total cpu time    =  0.108E-01 s
CCSOL: Total wall time   =  0.100E-01 s
Doubles amplitudes norm  =  0.8133479E-01
Total amplitudes norm    =  0.8133479E-01
Corr. energy             =     -0.0173541393
Number of CC iterations  =    1


 JAC ********************************************************
 JAC        Information for Jacobian eigenvalue solver       
 JAC        ------------------------------------------       
 JAC 
 JAC Number of eigenvalues                 1
 JAC Initial subspace dimension            1
 JAC Maximum subspace dimension                     9
 JAC 
 JAC Start guess for eigenvalues
 JAC ---------------------------
 JAC       1    0.8357185644       (singles ex.)
 JAC 
 JAC ********************************************************
 JAC
 JAC Jacobian eigenvalue solver
 JAC
 JAC Which   Subspace     Eigenvalue           Residual       Conv?  
 JAC     1       1        0.57888903         0.23134273        F
 JAC     1       2        0.56427001         0.11638147E-01    F
 JAC     1       3        0.56408078         0.23184420E-02    F
 JAC     1       4        0.56408192         0.11284465E-14    T


 ******************************************************************************
 *                        MP2      excitation energies                        *
 ******************************************************************************

       Exci.    Hartree           eV            cm-1           ||T1|| (%)
        1      0.5640819       15.34946       123801.7         99.21



 MP2 correlation energy :    -0.1735413931E-01



 *****************************************************************************
 *                      Full MP2 calculation is done !                       *
 *****************************************************************************





   ******************************************************************************
   *                             CC ENERGY SUMMARY                              *
   ******************************************************************************

     E: Hartree-Fock energy                            :       -1.1267867121
     E: Correlation energy                             :       -0.0173541393
     E: Total MP2 energy                               :       -1.1441408514



 CC Memory summary
 -----------------
 Allocated memory for array4   :  0.1072E-02 GB
 Memory in use for array4      :   0.000     GB
 Max memory in use for array4  :  0.5840E-05 GB
 ------------------


------------------------------------------------------
Total CPU  time used in CC           :        0.326645     s
Total Wall time used in CC           :        0.327000     s
------------------------------------------------------


Hostname       : pablo-AU                                          
Job finished   : Date: 14/12/2015   Time: 18:23:31



=============================================================================
                          -- end of CC program --
=============================================================================



 >>>  CPU Time used in *SCF is   0.47 seconds
 >>> wall Time used in *SCF is   0.47 seconds

Total no. of matmuls used:                       216
Total no. of Fock/KS matrix evaluations:           4
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Memory statistics          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (TOTAL):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(matrix)):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (real):                      0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MPI):                       0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (complex):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integer):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (logical):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (character):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (AOBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSAOTENSOR):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (SLSAOTENSOR):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (GLOBALLSAOTENSOR):          0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMTYPEITEM):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMITEM):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSMATRIX):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECORBITAL):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECFRAG):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlapType):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (BATCHTOORB):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECAOBATCHINFO):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MYPOINTER):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (fragmentAOS):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY2):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY4):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (PNOSpaceInfo):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2DENS):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (TRACEBACK):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2GRAD):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lvec_data)):           0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lattice_cell)):        0 byte  - Should be zero - otherwise a leakage is present
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Additional Memory information          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (linkshell):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integrand):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integralitem):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (intwork):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlap):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODitem):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (lstensor):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (FMM   ):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (XC    ):                    0 byte  - Should be zero - otherwise a leakage is present
  Max allocated memory, TOTAL                        853.982 MB
  Max allocated memory, type(matrix)                   3.840 kB
  Max allocated memory, real(realk)                  800.075 MB
  Max allocated memory, MPI                            0.000 Byte
  Max allocated memory, complex(complexk)              0.000 Byte
  Max allocated memory, integer                        9.505 kB
  Max allocated memory, logical                        0.728 kB
  Max allocated memory, character                      1.856 kB
  Max allocated memory, AOBATCH                       10.240 kB
  Max allocated memory, DECORBITAL                     0.000 Byte
  Max allocated memory, DECFRAG                        0.000 Byte
  Max allocated memory, BATCHTOORB                     0.112 kB
  Max allocated memory, DECAOBATCHINFO                 0.000 Byte
  Max allocated memory, MYPOINTER                      0.000 Byte
  Max allocated memory, fragmentAOS                    0.000 Byte
  Max allocated memory, ARRAY2                         0.000 Byte
  Max allocated memory, ARRAY4                         0.000 Byte
  Max allocated memory, ARRAY                         53.760 MB
  Max allocated memory, PNOSpaceInfo                   0.000 Byte
  Max allocated memory, MP2DENS                        0.000 Byte
  Max allocated memory, TRACEBACK                      0.000 Byte
  Max allocated memory, MP2GRAD                        0.000 Byte
  Max allocated memory, ODBATCH                        1.760 kB
  Max allocated memory, LSAOTENSOR                     4.864 kB
  Max allocated memory, SLSAOTENSOR                    4.784 kB
  Max allocated memory, GLOBALLSAOTENSOR               0.000 Byte
  Max allocated memory, ATOMTYPEITEM                  38.264 kB
  Max allocated memory, ATOMITEM                       1.024 kB
  Max allocated memory, LSMATRIX                       1.120 kB
  Max allocated memory, OverlapT                      29.536 kB
  Max allocated memory, linkshell                      0.432 kB
  Max allocated memory, integrand                     64.512 kB
  Max allocated memory, integralitem                  46.080 kB
  Max allocated memory, IntWork                       10.392 kB
  Max allocated memory, Overlap                       74.648 kB
  Max allocated memory, ODitem                         1.280 kB
  Max allocated memory, LStensor                       5.039 kB
  Max allocated memory, FMM                            0.000 Byte
  Max allocated memory, XC                             0.000 Byte
  Max allocated memory, Lvec_data                      0.000 Byte
  Max allocated memory, Lattice_cell                   0.000 Byte
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

  Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
  This is a non MPI calculation so naturally no memory is allocated on slaves!
 >>>  CPU Time used in LSDALTON is   0.48 seconds
 >>> wall Time used in LSDALTON is   0.48 seconds

End simulation
     Date and time (Linux)  : Mon Dec 14 18:23:31 2015
     Host name              : pablo-AU                                
