#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_energy_Si.info <<'%EOF%'
   linsca_energy_Si
   -------------
   Molecule:         Si atom
   Wave Function:    DFT/KT3
   Test Purpose:     Check Linsca 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_energy_Si.mol <<'%EOF%'
BASIS
cc-pVDZ
Silicon atom
Distance in Aangstroms
   1     0         *
       14.    1
Si     0.00000   0.00000   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_energy_Si.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
.FORCEGCBASIS
**INTEGRAL
.LINSCAPRINT
2
**WAVE FUNCTIONS
.DFT
KT3
**INFO
.PRINTMEMORY
.PRINTMEMORYLOWERLIMIT
1000000
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_energy_Si.check
cat >> linsca_energy_Si.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final DFT energy: * \-291\.634320[7-8]" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
