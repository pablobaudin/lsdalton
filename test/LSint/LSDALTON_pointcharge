#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_pointcharge.info <<'%EOF%'
   LSDALTON_pointcharge
   -------------
   Molecule:         water/3-21G
   Wave Function:    B3LYP
   Test Purpose:     Check pointcharge feature
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_pointcharge.mol <<'%EOF%'
ATOMBASIS
title
Warning Pointcharges must come last in molecule input
Atomtypes=6 Charge=0 Spherical Nosymmetry
Charge=8 Atoms=1 Basis=3-21G
o1 0.0000000000 0.0000000000 0.0000000000
Charge=1 Atoms=1 Basis=3-21G
h2 0.0000000000 -1.4207748912 1.0737442022
Charge=1 Atoms=1 Basis=3-21G
h3 -0.0000000000 1.4207748912 1.0737442022
Charge=-0.7960000000 Atoms=1 pointcharge
bq1 -4.7459987607 0.0000000000 -2.7401036621
Charge=0.3980000000 Atoms=1 pointcharge
bq2 -3.1217528345 0.0000000000 -2.0097934033
Charge=0.3980000000 Atoms=1 pointcharge
bq3 -4.4867611522 0.0000000000 -4.5020127872
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_pointcharge.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
**WAVE FUNCTIONS
.DFT
 B3LYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.CONVDYN
TIGHT
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_pointcharge.check
cat >> LSDALTON_pointcharge.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Final DFT energy:  * -76.24226448" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
