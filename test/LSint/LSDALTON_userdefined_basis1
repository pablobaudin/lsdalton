#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_userdefined_basis1.info <<'%EOF%'
   LSDALTON_userdefined_basis1
   -------------
   Molecule:         water/6-31G**
   Wave Function:    B3LYP
   Test Purpose:     test the option for userdefined atombasis
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_userdefined_basis1.mol <<'%EOF%'
ATOMBASIS
LSint test, H2O
6-31G** provided through userdefined ATOMBASIS
Atomtypes=2 Nosymmetry 
Charge=8.0  Atoms=1   Bas=USERDEFINED
O     0.0000000000000000   -0.2249058930  0.00000000
Charge=1.0  Atoms=2   Bas=6-31G**
H     1.4523500000000000    0.8996230000  0.00000000
H    -1.4523500000000000    0.8996230000  0.00000000

USERDEFINED BASIS
a 8
$ OXYGEN       (10s,4p) -> [3s,2p]                               
$ S-TYPE FUNCTIONS
   10    3    0
   5484.6717000  0.00183110  0.00000000  0.00000000
    825.2349500  0.01395010  0.00000000  0.00000000
    188.0469600  0.06844510  0.00000000  0.00000000
     52.9645000  0.23271430  0.00000000  0.00000000
     16.8975700  0.47019300  0.00000000  0.00000000
      5.7996353  0.35852090  0.00000000  0.00000000
     15.5396160  0.00000000 -0.11077750  0.00000000
      3.5999336  0.00000000 -0.14802630  0.00000000
      1.0137618  0.00000000  1.13076700  0.00000000
      0.2700058  0.00000000  0.00000000  1.00000000
$ P-TYPE FUNCTIONS
    4    2    0
     15.5396160  0.07087430  0.00000000
      3.5999336  0.33975280  0.00000000
      1.0137618  0.72715860  0.00000000
      0.2700058  0.00000000  1.00000000
$ D-TYPE FUNCTIONS
    1    1    0
      0.8000000  1.00000000

%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_userdefined_basis1.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
 B3LYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_userdefined_basis1.check
cat >> LSDALTON_userdefined_basis1.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Final DFT energy:  * -76.380917142" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
