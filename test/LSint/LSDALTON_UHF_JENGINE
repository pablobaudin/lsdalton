#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_UHF_JENGINE.info <<'%EOF%'
   LSDALTON_UHF_JENGINE
   -------------
   Molecule:         5 HCN molecules placed 20 atomic units apart
   Wave Function:    B3LYP/STO-2G
   Test Purpose:     Check UHF_JENGINE integrals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_UHF_JENGINE.mol <<'%EOF%'
BASIS
6-31G
HeH, CCSD( T)/cc-pVQZ opt. geometry
Test open shell response
Atomtypes=2 Charge=0 Nosymmetry Angstrom
Charge=1.0 Atoms=1
H     0.0000000000   0.0000000000   0.0000000000
Charge=2.0 Atoms=1
He    0.0000000000   0.0000000000   3.9218840000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_UHF_JENGINE.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_UHF_JENGINE.check
cat >> LSDALTON_UHF_JENGINE.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-3\.3533941[4-5]" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
