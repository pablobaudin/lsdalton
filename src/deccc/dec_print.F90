!> @file
!> Printing module for DEC subroutines
!> \author Thomas Kjaergaard
module dec_print_module
  use precision
  use fundamental! bohr_to_angstrom
  use dec_typedef_module

private

public :: PrintPairInfo

contains
!> \brief Print Info about the estimates 
!> \author Thomas Kjaergaard
!> \date February 2016
  subroutine PrintPairInfo(natoms,jobs,FragEnergies,DistanceTable,Esti)
    implicit none
    !> number of atoms/frags
    integer,intent(in) :: natoms
    !> Fragment job list
    type(joblist),intent(inout) :: jobs
    !> Distances between all atoms (a.u.)
    real(realk),intent(in) :: DistanceTable(natoms,natoms)
    ! Fragment energies 
    real(realk),intent(in) :: FragEnergies(natoms,natoms)
    ! Estimated Pair fragment or standard Pairs
    logical :: Esti
    !
    integer :: i,iatom,jatom
    real(realk) :: thr
    thr=1.0E-15_realk
    if(jobs%njobs<1)return
    IF(esti)THEN
       write(DECinfo%output,'(4X,a)') '*********************************************************************'
       write(DECinfo%output,'(4X,2a)') ' Estimated Pair fragment statistics '
       write(DECinfo%output,'(4X,a)') '*********************************************************************'
    ELSE
       write(DECinfo%output,'(4X,a)') '*********************************************************************'
       write(DECinfo%output,'(4X,2a)') ' Pair fragment statistics '
       write(DECinfo%output,'(4X,a)') '*********************************************************************'
    ENDIF
    write(DECinfo%output,'(4X,A4,1X,A4,1X,A4,1X,A4,1X,A4,1X,A5,1X,A5,3X,A7,1X,A9,14X,A6,1X,A13)') 'Atom','Atom','#occ', &
         & '#vir', '#bas', ' #aux', 'nodes', 'Time(s)', 'Dist(Ang)', 'Energy', 'DegreeOverlap'
    do i = 1, jobs%njobs
     iatom = jobs%atom1(i)
     jatom = jobs%atom2(i)
     if(abs(FragEnergies(iatom,jatom)) > thr  ) then
      write(DECinfo%output,'(4X,I4,1X,I4,1X,I4,1X,I4,1X,I4,1X,I5,1X,I5,F9.2,1X,F10.3,1X,g19.10,1X,F13.3)')  & 
           &iatom, jatom, jobs%nocc(i), jobs%nvirt(i), jobs%nbasis(i),jobs%nauxbasis(i),&
           & jobs%nslaves(i), jobs%LMtime(i), bohr_to_angstrom*DistanceTable(iatom,jatom), &
           & FragEnergies(iatom,jatom), jobs%DegreeOfOverlap(i)
     endif
    enddo

  end subroutine PrintPairInfo



end module dec_print_module
