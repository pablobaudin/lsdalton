module arh_finite_difference
use Fock_evaluator
use chol_module !what is this where is it and where is it needed?
use scf_utilities
use arh_module

                     integer, save :: neapprox
contains

   !Get e_extra = 
   ! - SUM(ijkl) Tr([D,X]*(Dk - D_0))*Tr(([D,X]*(Dl - D_0)) *Tr(Di - D0)*(F(Dj)-F(D0))*T_ik^(-1)*T_jl^(-1)
   ! + 2*SUM(ij) Tr([D,X]*(Di - D_0))*Tr(([D,X]*(F(Dj)-F(D0))*T_ij^(-1) 
   subroutine e_extra(x, fifoqueue, e_ext)
   implicit none
        !integer, intent(in)     :: pos
        integer                  :: i, j, k, l, matdim, ndens, ii, jj, kk, ll, error
        real(realk)              :: inv_ik, inv_jl, inv_ij, TrcomF, TrcomDi, &
                                  & TrDF, TrcomDk, TrcomDl, TrYF
        real(realk), intent(out) :: e_ext
        real(realk)              :: e_ext1, e_ext2, e_ext_idem
        TYPE(matrix), intent(in) :: x
        TYPE(matrix)             :: wrk1, wrk2, com, Fdiff, Ddiffi, Ddiffk, Ddiffl, Yi
        TYPE(matrix), pointer    :: F0, D0, Fj, Di, Dk, Dl, dummy
        !TYPE(util_HistoryStore), intent(in) :: queue
        type(modFIFO)            :: fifoqueue

   ndens = fifoqueue%offset
   matdim = x%nrow

   call MAT_INIT(wrk1,matdim,matdim)
   call MAT_INIT(wrk2,matdim,matdim)
   call MAT_INIT(com,matdim,matdim)
   call MAT_INIT(Fdiff,matdim,matdim)
   call MAT_INIT(Ddiffi,matdim,matdim)
   call MAT_INIT(Ddiffk,matdim,matdim)
   call MAT_INIT(Ddiffl,matdim,matdim)
   !Get [D,X]:
   call MAT_MUL(fifoqueue%D_exp,x,'n','n',1.0E0_realk,0.0E0_realk,wrk1)
   call mat_trans(wrk1,wrk2)
   call mat_scal(-1.0E0_realk,wrk2)
   call MAT_ADD(1.0E0_realk, wrk1, -1.0E0_realk, wrk2, com)

   e_ext = 0
   e_ext1 = 0
   e_ext2 = 0
   e_ext_idem = 0

   call get_from_modFIFO(fifoqueue, 0, F0, D0)
   do j = 1, ndens
      !Fdiff = F(D(j)) - F(D0)
      call get_from_modFIFO(fifoqueue, j, Fj, dummy)
      call MAT_ADD(1.0E0_realk, Fj, -1.0E0_realk, F0, Fdiff)
      !TrcomF = Tr(([D,X]*(F(Dj)-F(D0))
      TrcomF = mat_trAB(com,Fdiff)
         do i = 1, ndens
         !Ddiffi = D(i) - D0
         call get_from_modFIFO(fifoqueue, i, dummy, Di)
         call MAT_ADD(1.0E0_realk, Di, -1.0E0_realk, D0, Ddiffi)
         !TrcomDi = Tr ([D,X]*(Di - D_0))
         TrcomDi = mat_trAB(com,Ddiffi)
         !TrDF = Tr(Di - D0)*(F(Dj)-F(D0)) 
         TrDF = mat_trAB(Ddiffi, Fdiff)
         !inv_ij = inv_metr(ii,jj)
         inv_ij = inv_fifometric(i,j) 
         e_ext2 = e_ext2 + inv_ij*TrcomDi*TrcomF
         do k = 1, ndens
            !Ddiffk = D(k) - D0
            call get_from_modFIFO(fifoqueue, k, dummy, Dk)
            call MAT_ADD(1.0E0_realk, Dk, -1.0E0_realk, D0, Ddiffk)
            inv_ik = inv_fifometric(i,k)
            !TrcomDk = Tr ([D,X]*(Dk - D_0))
            TrcomDk = mat_trAB(com,Ddiffk)            
            do l = 1, ndens
               inv_jl = inv_fifometric(j,l)
               !Ddiffi = D(l) - D0
               call get_from_modFIFO(fifoqueue, l, dummy, Dl)
               call MAT_ADD(1.0E0_realk, Dl, -1.0E0_realk, D0, Ddiffl)
               !TrcomDi = Tr ([D,X]*(Di - D_0))
               TrcomDl = mat_trAB(com,Ddiffl)
               e_ext1 = e_ext1 + inv_jl*inv_ik*TrcomDl*TrcomDk*TrDF
               e_ext_idem = e_ext_idem + inv_jl*inv_ik*TrcomDl*TrcomDk*TrYF
            enddo
         enddo
      enddo
   enddo

    e_ext = 2*e_ext2 - e_ext1

   call MAT_FREE(wrk1)
   call MAT_FREE(wrk2)
   call MAT_FREE(com)
   call MAT_FREE(Fdiff)
   call MAT_FREE(Ddiffi)
   call MAT_FREE(Ddiffk)
   call MAT_FREE(Ddiffl)
   end subroutine e_extra

   !Get E_epsilon = 2*Tr(F(D_0)*D(X))
   subroutine e_epsilon(Dchol, e_eps) 
   implicit none
        integer                  :: i, j
        TYPE(matrix), intent(in) :: Dchol
        real(realk)              :: trace
        real(realk), intent(out) :: e_eps
   
   trace = mat_trAB(FUone,Dchol)
   e_eps = 2.0E0_realk*trace
   end subroutine e_epsilon

   subroutine e_appr(X, fifoqueue, e_approx)
   implicit none

        !integer, intent(in)     :: pos
        real(realk)              :: e_ext, e_eps
        real(realk), intent(out) :: e_approx
        TYPE(matrix), intent(in) :: X
        TYPE(matrix)             :: wrk, Dchol, X_proj, F
        !TYPE(util_HistoryStore), intent(in) :: queue
        type(modFIFO)            :: fifoqueue
        integer                  :: ndim
        logical, external        :: do_dft

   ndim = X%nrow
   neapprox = neapprox + 1

   call MAT_INIT(X_proj, ndim, ndim)
   call MAT_INIT(Dchol, ndim, ndim)
   call MAT_INIT(wrk, ndim, ndim)
   call MAT_INIT(F, ndim, ndim)

   call chol_PROJECT_cholbasis(x, 2, X_proj)   
   call chol_density_param(x_proj,wrk) !TAYLOR!
   call chol_purify(wrk,Dchol)
   !if (debug_hessian_exact) then
   !   !transform density to AO basis
   !   call x_from_chol_basis(Dchol, wrk)
   !   !get energy
   !   call FCK_get_fock(wrk, F, e_ext)
   !   e_eps = 0.0E0_realk
   !else
      if (cfg_arhterms) then
         call e_epsilon(Dchol, e_eps)
         call e_extra(X_proj, fifoqueue, e_ext)
      else
         call e_epsilon(Dchol, e_eps)
         e_ext = 0.0E0_realk
      endif
   !endif

   e_approx = e_eps + e_ext

   call MAT_FREE(X_proj)
   call MAT_FREE(Dchol)
   call MAT_FREE(wrk)
   call MAT_FREE(F)
   end subroutine e_appr

   subroutine finite_diff(fifoqueue, grad, hes)
   implicit none
   
        !integer, intent(in)      :: pos
        integer                  :: i, j, k, matdim, vecdim, ndens
        real(realk)              :: e_plus1, e_minus1, e_plus2, e_minus2, e0, &
                                  & e_p1p1, e_m1m1, e_p1m1, e_m1p1, &
                                  & e_p2p2, e_m2m2, e_p2m2, e_m2p2, G1, G2, & 
                                  & small, gradient_contrib, diag_hes, non_diag_hes
        TYPE(matrix)             :: X_trial_vec, X_trial_mat, gradient, hessian
        TYPE(matrix)             :: grad, hes !Output
        !TYPE(util_HistoryStore), intent(in) :: queue
        type(modFIFO)            :: fifoqueue
        real(realk)              :: metr, delm, felm !DEBUG
   
      !write (LUPRI,*) "FU in finite difference:"
      !call MAT_PRINT(FU, 1, FU%nrow, 1, FU%ncol, LUPRI)
   !ndens = queue%used_entries
   neapprox = 0

   matdim = fifoqueue%D_exp%nrow
   vecdim = grad%nrow

   small = 10.0E-4_realk
   call MAT_INIT(X_trial_vec,vecdim,1)
   call MAT_INIT(X_trial_mat,matdim,matdim)
   call MAT_INIT(gradient,vecdim,1)
   call MAT_INIT(hessian,vecdim,vecdim)

   call MAT_ZERO(gradient)
   call MAT_ZERO(hessian)
   !Gradient and diagonal hessian elements:
   !write (LUPRI,*) "vecdim =", vecdim ; call flshfo(lupri)
 
   do i = 1, vecdim
      !write (LUPRI,*) "i =", i ; call flshfo(lupri)
      call MAT_ZERO(X_trial_vec)
      call MAT_ZERO(X_trial_mat)
      !Get E_appr(X=0):
      call e_appr(X_trial_mat, fifoqueue, e0)
      !write (LUPRI,*) "e0: ", e0 ; call flshfo(lupri)
      call mat_create_elm(i, 1, small, X_trial_vec)
      call mat_VEC_TO_MAT('a', X_trial_vec, X_trial_mat)
       
      call e_appr(X_trial_mat, fifoqueue, e_plus1)
      !write (LUPRI,*) "e+1: ", e_plus1 ; call flshfo(lupri) 

      call mat_scal(-1.0E0_realk, X_trial_mat)
      call e_appr(X_trial_mat, fifoqueue, e_minus1)
      !write (LUPRI,*) "e-1: ", e_minus1 ; call flshfo(lupri)

      call mat_scal(2.0E0_realk, X_trial_mat)
      call e_appr(X_trial_mat, fifoqueue, e_minus2)
      !write (LUPRI,*) "e-2: ", e_minus2 ; call flshfo(lupri)

      call mat_scal(-1.0E0_realk, X_trial_mat)
      call e_appr(X_trial_mat, fifoqueue, e_plus2)
      !write (LUPRI,*) "e+2 ", e_plus2 ; call flshfo(lupri)

   !Calculate gradient:
      !gradient_contrib = (e_plus1-e_minus1)/(2.0E0_realk*small) !gradient to lower order
      gradient_contrib = (8.0E0_realk*(e_plus1-e_minus1) - (e_plus2-e_minus2))/(12.0E0_realk*small)
      call mat_create_elm(i, 1, gradient_contrib, gradient)     

   !Calculate diagonal hessian elements:
      diag_hes = (16.0E0_realk*(e_plus1+e_minus1-2.0E0_realk*e0) - (e_plus2+e_minus2-2.0E0_realk*e0))/(12.0E0_realk*small**2)
      call mat_create_elm(i, i, diag_hes, hessian)
   enddo

   write (lupri,*) 'Number of calls to e_approx in 1st loop finite difference:', neapprox
   neapprox = 0
   
   !Non-diagonal hessian elements:
   do i = 1, vecdim
      do j = 1, i-1
         call MAT_ZERO(x_trial_vec)
         call MAT_ZERO(x_trial_mat)

      !Ep1p1:
         call mat_create_elm(j, 1, small, X_trial_vec)
         call mat_create_elm(i, 1, small, X_trial_vec) 
         call mat_VEC_TO_MAT('a', X_trial_vec, X_trial_mat) 
         call e_appr(X_trial_mat, fifoqueue, e_p1p1)
      !Em1m1:
         call mat_scal(-1.0E0_realk,X_trial_mat)
         call e_appr(X_trial_mat, fifoqueue, e_m1m1)

         call MAT_ZERO(x_trial_vec)
         call MAT_ZERO(x_trial_mat)

      !Ep1m1:
         call mat_create_elm(j, 1, small, X_trial_vec)
         call mat_create_elm(i, 1, -small, X_trial_vec) 
         call mat_VEC_TO_MAT('a', X_trial_vec, X_trial_mat) 
         call e_appr(X_trial_mat, fifoqueue, e_p1m1)
      !Em1p1:
         call mat_scal(-1.0E0_realk,X_trial_mat)
         call e_appr(X_trial_mat, fifoqueue, e_m1p1)

         call MAT_ZERO(x_trial_vec)
         call MAT_ZERO(x_trial_mat)

      !Ep2p2:
         call mat_create_elm(j, 1, 2.0E0_realk*small, X_trial_vec)
         call mat_create_elm(i, 1, 2.0E0_realk*small, X_trial_vec) 
         call mat_VEC_TO_MAT('a', X_trial_vec, X_trial_mat) 
         call e_appr(X_trial_mat, fifoqueue, e_p2p2)
      !Em2m2:
         call mat_scal(-1.0E0_realk,X_trial_mat)
         call e_appr(X_trial_mat, fifoqueue, e_m2m2)

         call MAT_ZERO(x_trial_vec)
         call MAT_ZERO(x_trial_mat)

      !Ep2m2:
         call mat_create_elm(j, 1, 2.0E0_realk*small, X_trial_vec)
         call mat_create_elm(i, 1, -2.0E0_realk*small, X_trial_vec) 
         call mat_VEC_TO_MAT('a', X_trial_vec, X_trial_mat) 
         call e_appr(X_trial_mat, fifoqueue, e_p2m2)
      !Em2p2:
         call mat_scal(-1.0E0_realk,X_trial_mat)
         call e_appr(X_trial_mat, fifoqueue, e_m2p2)

      G1 = e_p1p1-e_p1m1-e_m1p1+e_m1m1
      G2 = e_p2p2-e_p2m2-e_m2p2+e_m2m2
 
      non_diag_hes = (16.0E0_realk*G1 - G2)/(48.0E0_realk*small**2)
      call mat_create_elm(i, j, non_diag_hes, hessian)
      call mat_create_elm(j, i, non_diag_hes, hessian)
      enddo
   enddo

   write (lupri,*) 'Number of calls to e_approx in 2nd loop finite difference:', neapprox

   !do k = 1, gradient%nrow*gradient%ncol
   !   if (ABS(gradient%elms(k)) < cfg_tiny) then
   !      gradient%elms(k) = 0.0E0_realk
   !   endif
   !enddo

!print *, ' gradient%nrow, ncol', gradient%nrow, gradient%ncol
!print *, ' grad%nrow, ncol', grad%nrow, grad%ncol
!print *, 'hes%nrow, hes%ncol', hes%nrow, hes%ncol
!print *, 'hessian%nrow, hessian%ncol', hessian%nrow, hessian%ncol
   grad = gradient
   hes = hessian

   call MAT_FREE(X_trial_vec)
   call MAT_FREE(X_trial_mat)
   call MAT_FREE(gradient)
   call MAT_FREE(hessian)
   end subroutine finite_diff

end module arh_finite_difference
