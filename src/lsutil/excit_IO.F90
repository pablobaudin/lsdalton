module excitation_IO

use precision
use matrix_module, only: matrix
use matrix_operations, only: mat_write_to_disk, mat_read_from_disk
use files, only: lsopen, lsclose
use memory_handling, only: mem_alloc,mem_dealloc

public :: write_excitation_energies_and_vectors,&
     & read_excitation_energies_and_vectors,&
     & write_lineq_vectors,read_lineq_vectors,read_nlineq_vectors

private
CONTAINS

!> Subroutine to write the calculated excitation energies and vectors to file
!> \param eivecs the excitation eigenvetors
!> \param eival the excitation energies
!> \param nexci the number of excitation
!> \param nexci the number of excitation
subroutine  write_excitation_energies_and_vectors(eivecs,eival,nexci)
implicit none
integer,intent(IN)      :: nexci
type(matrix),intent(IN) :: eivecs(nexci)
real(realk),intent(IN)  :: eival(nexci)
!
logical :: onMaster
integer :: lurestart,i
   
OnMaster=.TRUE.
lurestart = -1

CALL LSOPEN(lurestart,'scf_rsp_eigenvecs_ao.restart','unknown','UNFORMATTED')
rewind(lurestart)
write(lurestart) int(nexci,kind=long)
do i = 1,nexci
   write(lurestart) int(i,kind=long), eival(i)
   call mat_write_to_disk(lurestart,eivecs(i),OnMaster)
enddo
CALL LSCLOSE(lurestart,'KEEP')
end subroutine  write_excitation_energies_and_vectors

!> Subroutine to read the calculated excitation energies and vectors to file
!> \param eivecs the excitation eigenvetors
!> \param eival the excitation energies
!> \param nexci the number of excitation
!> \param nexci the number of excitation
!> \param selectedExci a selected set of excitation enerergies (assumed to be ordered from low to high)
!> \param delete the number of excitation
subroutine read_excitation_energies_and_vectors(eivecs,eival,nexci,selectedExci,delete)
implicit none
integer,intent(IN)         :: nexci
type(matrix),intent(INOUT) :: eivecs(nexci)
real(realk),intent(OUT)    :: eival(nexci)
integer,optional,target    :: selectedExci(nexci)
logical,optional           :: delete
!
logical :: onMaster,doDelete
integer :: lurestart,i,max_exci,i_current
integer(kind=long) :: nexci_disk,i_long
Integer,pointer    :: exci(:)
   
doDelete = .FALSE.
IF (present(delete)) doDelete = delete

max_exci = nexci
IF (present(selectedExci)) THEN
  DO i=2,nexci
    IF (selectedExci(I) .LE. selectedExci(i-1)) CALL &
    & LSQUIT('Error in read_excitation_energies_and_vectors. selectedExci not ordered from low to high.',-1)
  ENDDO
  max_exci = selectedExci(nexci)
  exci => selectedExci
ELSE
  call mem_alloc(exci,nexci)
  DO i=1,nexci
   exci(i) = i
  ENDDO
ENDIF

OnMaster=.TRUE.
lurestart = -1

CALL LSOPEN(lurestart,'scf_rsp_eigenvecs_ao.restart','unknown','UNFORMATTED')
rewind(lurestart)
read(lurestart) nexci_disk
IF (nexci_disk.LT.int(max_exci,kind=long)) THEN
  call lsquit('Error in read_excitation_energies_and_vectors. Asking for too many/high eigenvectors/-values.',-1)
ENDIF

!In the case of selectedExci use the next eigenvector/-value to read intermediate vectors/values
i_current = 1
do i = 1,max_exci
   read(lurestart) i_long, eival(i_current)
   call mat_read_from_disk(lurestart,eivecs(i_current),OnMaster)
   IF (exci(i_current).EQ.i) THEN 
     i_current = i_current+1
   ENDIF
enddo

IF (.NOT.present(selectedExci)) call mem_dealloc(exci)

IF (doDelete) THEN
  print*,'REMOVE solutions to disk for restart molcfg%solver%rsp_restart_exci'
  CALL LSCLOSE(lurestart,'DELETE')
ELSE
  CALL LSCLOSE(lurestart,'KEEP')
ENDIF
end subroutine  read_excitation_energies_and_vectors

!> Subroutine to write the calculated response vectors to file
!> \param eivecs the response vector
!> \param nexci the number of response vector
subroutine  write_lineq_vectors(nexci,eivecs)
implicit none
integer,intent(IN)      :: nexci
type(matrix),intent(IN) :: eivecs(nexci)
!
logical :: onMaster,fileexists2
integer :: lurestart,i
integer(kind=long) :: nexci_disk,nexciOLD
INQUIRE(file='scf_rsp_lineqvecs_ao.restart',EXIST=fileexists2)
OnMaster=.TRUE.
lurestart = -1
IF(fileexists2)THEN
   CALL LSOPEN(lurestart,'scf_rsp_lineqvecs_ao.restart','OLD','UNFORMATTED')
   rewind(lurestart)
   read(lurestart) nexciOLD
   rewind(lurestart)
   nexci_disk = nexci+nexciOLD
   write(lurestart) nexci_disk
   CALL LSCLOSE(lurestart,'KEEP')
   CALL LSOPEN(lurestart,'scf_rsp_lineqvecs_ao.restart','OLD','UNFORMATTED',POSIN='APPEND')
ELSE
   CALL LSOPEN(lurestart,'scf_rsp_lineqvecs_ao.restart','unknown','UNFORMATTED')
   nexci_disk = nexci
   write(lurestart) nexci_disk
ENDIF
do i = 1,nexci
   call mat_write_to_disk(lurestart,eivecs(i),OnMaster)
enddo
CALL LSCLOSE(lurestart,'KEEP')
end subroutine  write_lineq_vectors

!> Subroutine to read the response vectors from file
!> \param eivecs the response vetors
!> \param nexci the number of respoonse
subroutine read_lineq_vectors(nexci,eivecs)
implicit none
integer,intent(IN)         :: nexci
type(matrix),intent(INOUT) :: eivecs(nexci)
!
logical :: onMaster,doDelete
integer :: lurestart,i,max_exci
integer(kind=long) :: nexci_disk,i_long
   
OnMaster=.TRUE.
lurestart = -1
CALL LSOPEN(lurestart,'scf_rsp_lineqvecs_ao.restart','unknown','UNFORMATTED')
rewind(lurestart)
read(lurestart) nexci_disk
IF (nexci_disk.NE.int(nexci,kind=long)) THEN
   call lsquit('Error in read_lineq vectors.',-1)
ENDIF
do i = 1,nexci
   call mat_read_from_disk(lurestart,eivecs(i),OnMaster)
enddo
CALL LSCLOSE(lurestart,'KEEP')
end subroutine  read_lineq_vectors

!> Subroutine to read the response vectors from file
!> \param eivecs the response vetors
!> \param nexci the number of respoonse
subroutine read_nlineq_vectors(nexci)
implicit none
integer,intent(INOUT)         :: nexci
!
logical :: onMaster
integer :: lurestart
integer(kind=long) :: nexci_disk
   
OnMaster=.TRUE.
lurestart = -1
CALL LSOPEN(lurestart,'scf_rsp_lineqvecs_ao.restart','unknown','UNFORMATTED')
rewind(lurestart)
read(lurestart) nexci_disk
nexci = nexci_disk
CALL LSCLOSE(lurestart,'KEEP')
end subroutine  read_nlineq_vectors

end module excitation_IO
