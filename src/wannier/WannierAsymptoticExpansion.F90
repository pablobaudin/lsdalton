!> @brief This module calculates the farrfield using the asymptotic
!> expansion according to: 
!> 	Yamada, T., Brewster, R. P., \& Hirata, S. (2013).
!> 	Asymptotic expansion of two-electron integrals and its application to 
!> 	Coulomb and exchange lattice sums in metallic, semimetallic, and 
!> 	nonmetallic crystals. J. Chem. Phys., 139, 0–13.

module wannier_asymptotic_expansion

  use precision
  use wannier_utils
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use wlattice_domains, only: &
       ltdom_init, &
       ltdom_free, &
       ltdom_getlayer, &
       lattice_domains_type, &
       ltdom_getdomain, &
       ltdom_getintersect, &
       ltdom_contains
  use lattice_storage, only: &
       lmatrixp, &
       llmatrixp, &
       linteger, &
       lmatrixarrayp, &
       lts_mat_init, &
       lts_init, &
       lts_free, &
       lts_get, &
       lts_axpy, &
       lts_store, &
       lts_load, &
       lts_zero, &
       lts_print, &
       lts_copy
  use matrix_module, only: &
       matrix
  use timings_module
  use memory_handling, only: &
       mem_alloc, mem_dealloc
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_dotproduct, &
       mat_abs_max_elm, &
       mat_trans, &
       mat_print
 

  private

  public :: wcalc_JZAE, wcalc_ZAE 

  type wae_midpoints
     integer, pointer :: midpt_indices(:, :)
     real(realk), pointer :: midpts(:, :)
     integer :: indxmax
  end type wae_midpoints

contains

  !> @brief Computes the asymptotic correction to the Coulomb and Z matrices and add it to the Fock matrix
  !> Implementation of the Coulomb farfield contribution to the
  !> Fock matrix using the Asymptotic expansion sceme of Yamada, T., Brewster, 
  !> R. P., \& Hirata, S. (2013),  J. Chem. Phys., 139.
  !> @author Elisa Rebolini
  !> @date Oct 2016
  !> @param[inout] fockmat AO Fock matrix in lattice storage
  !> @param[in] smat AO overlap matrix in lattice storage
  !> @param[in] dens AO density matrix in lattice storage
  !> @param[in] refcell Molecule info for the reference cell
  !> @param[in] wconf config parameters relevant to wannier scf.
  !> @param[in] lupri Logical unit for dalton standard output.
  !> @param[in] lupri Logical print unit for error.
  subroutine wcalc_JZAE(fockmat,smat,dens,refcell,wconf,lupri,luerr)

    type(lmatrixp), intent(inout)    :: fockmat
    type(lmatrixp),intent(in)        :: smat,dens
    type(moleculeinfo), intent(in)   :: refcell
    type(wannier_config), intent(in) :: wconf
    integer,intent(in)               :: lupri,luerr

    integer                     :: nbas, natoms, layer, nf_cutoff(3),ff_cutoff(3)
    real(realk)                 :: R_M1(3), R_M2(3), R_M3(3), R_A(3), R_AB(3), R_CD(3), Rtotal(3)
    integer                     :: m1, indx_m1, m2, indx_m2, m3, indx_m3
    integer                     :: a, b, c, d, mu, nu, munu, i
    integer                     :: StartRegC, EndRegC, StartRegD, EndRegD
    type(lattice_domains_type)  :: dom_s, dom_f, dom_ff
    type(lmatrixp)              :: Satom
    type(matrix)                :: S_0M1, J_0M1, SAtom_M3
    type(matrix),pointer        :: F_0M1
    logical                     :: lstat_S, lstat_F
    real(realk)                 :: S_AB,tmpsum, Z_A

    nbas = dens%nrow
    natoms = refcell%natoms

    nf_cutoff(:) = wconf%nf_cutoff(:)
    ff_cutoff(:) = wconf%ff_cutoff(:)

    !Set up the domains
    call ltdom_init(dom_s, wconf%blat%dims, maxval(smat%cutoffs))
    call ltdom_getdomain(dom_s, smat%cutoffs, incl_redundant=.true.)
    call ltdom_init(dom_f, wconf%blat%dims, maxval(fockmat%cutoffs))
    call ltdom_getdomain(dom_f, fockmat%cutoffs, incl_redundant=wconf%no_redundant)
    
    call lts_init(Satom, wconf%blat%dims, smat%cutoffs, natoms, natoms, wconf%no_redundant) 
    
    call get_atomic_overlap(SAtom,refcell,smat,dens,wconf,lupri,luerr)

    call mat_init(S_0M1,nbas,nbas)
    call mat_init(J_0M1,nbas,nbas)
    call mat_init(SAtom_M3,natoms,natoms)
    
    !Loop over cell in fock cutoff
    loopm1: do m1=1,dom_f%nelms
       indx_m1 = dom_f%indcs(m1)
       F_0M1 => lts_get(fockmat, -indx_m1)
       if (.not.(associated(F_0M1))) cycle
       call mat_zero(J_0M1)
       call lts_copy(smat, indx_m1, lstat_S, S_0M1)
       if (.not.(lstat_s)) cycle
       R_M1 = wu_indxtolatvec(wconf%blat, indx_m1)

       StartRegC=1
       !loop over atom C in the ref cell
       loopC: do c = 1,natoms
          EndRegC = StartRegC + refcell%atom(c)%nContOrbREG - 1
          !loop over D in the ref cell
          StartRegD=1
          loopd: do d = 1, natoms
             EndRegD = StartRegD + refcell%atom(d)%nContOrbREG - 1
             R_CD(1:3) = (refcell%atom(d)%center(1:3) + refcell%atom(c)%center(1:3))/2

             tmpsum = 0.0_realk

             !loop over cell in the far-field
             !loop over layers
             loopff: do layer = maxval(nf_cutoff)+1, maxval(ff_cutoff)
                call ltdom_init(dom_ff, wconf%blat%dims, maxval(ff_cutoff))
                call ltdom_getlayer(dom_ff, layer, .true.)
                loopm2: do m2=1,dom_ff%nelms
                   indx_m2 = dom_ff%indcs(m2)
                   R_M2 = wu_indxtolatvec(wconf%blat, indx_m2)
                   
                   !loop over min of S and D cutoff
                   loopm3: do m3 = 1,dom_s%nelms
                      indx_m3 = dom_s%indcs(m3)
                      R_M3 = wu_indxtolatvec(wconf%blat, indx_m3)
                      call lts_copy(Satom, indx_m3, lstat_S, SAtom_M3)
                      if (.not.(lstat_s)) cycle
                      !loop over atom A in the ref cell
                      loopA: do a = 1, natoms
                         !loop over atom B in the ref cell
                         loopB : do b = 1, natoms
                            R_AB(1:3) = (refcell%atom(a)%center(1:3) + &
                                 refcell%atom(b)%center(1:3))/2
                            S_AB  = SAtom_M3%elms((b-1)*natoms + a) 
                            do i=1,3
                               Rtotal(i) = R_CD(i) - R_AB(i) - R_M2(i) + (R_M1(i) - R_M3(i))/2
                            enddo
                            tmpsum = tmpsum + S_AB / dnorm2(Rtotal,3)
                        
                         enddo loopB
                      enddo loopA
                   enddo loopm3

                   !loop over atom A in the ref cell
                   loopAZ: do a = 1, natoms
                      R_A(1:3) = refcell%atom(a)%center(1:3)
                      Z_A = refcell%atom(a)%charge
                                            
                      Rtotal(:) = R_CD(:) - R_A(:) - R_M2(:) + (R_M1(:))/2
                      tmpsum = tmpsum - Z_A / dnorm2(Rtotal,3)
                   enddo loopAZ
                                      
                enddo loopm2
                call ltdom_free(dom_ff)
             enddo loopff
                
             !loop over AO mu on atom C
             loopmu: do mu = StartRegC,EndRegC
                !loop over AO nu on atom D
                loopnu: do nu = StartRegD,EndRegD
                   munu = (nu-1)*nbas + mu
                   J_0M1%elms(munu) = J_0M1%elms(munu) + S_0M1%elms(munu) * tmpsum
                enddo loopnu
             enddo loopmu
             StartRegD = EndRegD + 1
          enddo loopd
          StartRegC = EndRegC + 1
       enddo loopC
 !      write(*,*) 'NF_0M1',indx_M1
 !      call mat_print(J_0M1,1,nbas,1,nbas,6)
       call mat_daxpy(1.0_realk,J_0M1,F_0M1)
    enddo loopm1

    call lts_free(Satom)
    
    call mat_free(S_0M1)
    call mat_free(J_0M1)
    call mat_free(SAtom_M3)
    
    call ltdom_free(dom_s)
    call ltdom_free(dom_f)
   
    
  end subroutine wcalc_JZAE

  !> @brief Computes the asymptotic correction to the nuclei-electron matrix and add it to the Fock matrix
  !> @author Elisa Rebolini
  !> @date Oct 2016
  !> @param[inout] fockmat AO Fock matrix in lattice storage
  !> @param[in] smat AO overlap matrix in lattice storage
  !> @param[in] refcell Molecule info for the reference cell
  !> @param[in] wconf config parameters relevant to wannier scf.
  !> @param[in] lupri Logical unit for dalton standard output.
  !> @param[in] lupri Logical print unit for error.
  subroutine wcalc_ZAE(fockmat,smat,refcell,wconf,lupri,luerr)

    type(lmatrixp), intent(inout)    :: fockmat
    type(lmatrixp),intent(in)        :: smat
    type(moleculeinfo), intent(in)   :: refcell
    type(wannier_config), intent(in) :: wconf
    integer,intent(in)               :: lupri,luerr

    integer                     :: nbas, natoms
    integer                     :: nf_cutoff(3), ff_cutoff(3)
    integer                     :: m1, indx_m1, m2, indx_m2
    integer                     :: a, c, d, layer
    real(realk)                 :: R_M1(3), R_M2(3), R_CD(3), R_A(3), Rtotal(3)
    real(realk)                 :: Z_A, sum
    integer                     :: mu, nu, munu
    integer                     :: StartRegC, EndRegC, StartRegD, EndRegD
    type(lattice_domains_type)  :: dom_f, dom_ff
    type(matrix)                :: S_0M1, Z_0M1
    type(matrix),pointer        :: F_0M1
    logical                     :: lstat_S, lstat_F
    
    nbas = fockmat%nrow
    natoms = refcell%natoms

    nf_cutoff(:) = wconf%nf_cutoff(:)
    ff_cutoff(:) = wconf%ff_cutoff(:)

!    write(*,*) 'NF cutoff',nf_cutoff
!    write(*,*) 'FF cutoff',ff_cutoff
    
    !Set up the domains
    call ltdom_init(dom_f, wconf%blat%dims, maxval(fockmat%cutoffs))
    call ltdom_getdomain(dom_f, fockmat%cutoffs, incl_redundant=.true.)

    call mat_init(Z_0M1,nbas,nbas)
    call mat_init(S_0M1,nbas,nbas)
    
    !Loop over cells in dom_f
    loop_m1: do m1=1,dom_f%nelms
       indx_m1 = dom_f%indcs(m1)
       F_0M1 => lts_get(fockmat, indx_m1)
       if (.not.(associated(F_0M1))) cycle
       call mat_zero(Z_0M1)

       call lts_copy(smat, indx_m1, lstat_S, S_0M1)
       if (.not.(lstat_S)) cycle
       R_M1 = wu_indxtolatvec(wconf%blat, indx_m1)

       StartRegC=1
       !loop over atom C in the ref cell
       loopC: do c = 1,natoms
          EndRegC = StartRegC + refcell%atom(c)%nContOrbREG - 1
          !loop over D in the ref cell
          StartRegD=1
          loopd: do d = 1, natoms
             EndRegD = StartRegD + refcell%atom(d)%nContOrbREG - 1
             R_CD(1:3) = (refcell%atom(d)%center(1:3) + refcell%atom(c)%center(1:3))/2

             sum = 0.0_realk
             
             !loop over cell in the far-field
             !loop over layers
             loopff: do layer = maxval(nf_cutoff)+1, maxval(ff_cutoff)
                !layer = maxval(nf_cutoff)
                call ltdom_init(dom_ff, wconf%blat%dims, maxval(ff_cutoff))
                call ltdom_getlayer(dom_ff, layer, .true.)
                loopm2: do m2=1,dom_ff%nelms
                   indx_m2 = dom_ff%indcs(m2)
                   R_M2 = wu_indxtolatvec(wconf%blat, indx_m2)
                
                   !loop over atom A in the ref cell
                   loopA: do a = 1, natoms
                      R_A(1:3) = refcell%atom(a)%center(1:3)
                      Z_A = refcell%atom(a)%charge
                                            
                      Rtotal(:) = R_CD(:) - R_A(:) - R_M2(:) + (R_M1(:))/2
                      sum = sum - Z_A / dnorm2(Rtotal,3)
                   enddo loopA
                   
                enddo loopm2
                call ltdom_free(dom_ff)
             enddo loopff
             
             !loop over AO mu on atom C
             loopmu: do mu = StartRegC,EndRegC
                !loop over AO nu on atom D
                loopnu: do nu = StartRegD,EndRegD
                   munu = (nu-1)*nbas + mu
                   Z_0M1%elms(munu) = Z_0M1%elms(munu) + S_0M1%elms(munu) * sum
                enddo loopnu
             enddo loopmu
             StartRegD = EndRegD + 1
          enddo loopd
          StartRegC = EndRegC + 1
       enddo loopC
       
       call mat_daxpy(1.0_realk,Z_0M1,F_0M1)
!       write(*,*) 'Z_0M1',indx_M1
!       call mat_print(Z_0M1,1,nbas,1,nbas,6)
    enddo loop_m1

    
    call mat_free(S_0M1)
    call mat_free(Z_0M1)
        
    call ltdom_free(dom_f)
    
  end subroutine wcalc_ZAE

  !> @brief Calculate the atomic overlaps as defined by Hirata Yamada, T., 
  !> Brewster, R. P., \& Hirata, S. (2013),  J. Chem. Phys., 139.
  !> @param[inout] SAtom AO the atomic overlaps
  !> @param[in] refcell Molecule info for the reference cell
  !> @param[in] smat AO overlap matrix in lattice storage
  !> @param[in] dens AO density matrix in lattice storage
  !> @param[in] wconf config parameters relevant to wannier scf.
  !> @param[in] lupri Logical unit for dalton standard output.
  !> @param[in] lupri Logical print unit for error.
  !> @author Karl R. L.
  !> @date October 2016
  !
  !
  ! TODO does only work in its current state ic smat, dens is stored
  ! with redundant=.true. FIX this to work in the general case.
  subroutine get_atomic_overlap(SAtom,refcell,smat,dens,wconf,lupri,luerr)
    
    type(lmatrixp),intent(inout)     :: Satom
    type(lmatrixp),intent(in)        :: smat,dens
    type(moleculeinfo),intent(in)    :: refcell
    type(wannier_config), intent(in) :: wconf
    integer, intent(in)              :: lupri,luerr

    integer :: i, m, indx_m, dens_indx, smat_indx, nbast, & 
         & natoms, a, b, mu, nu, muc, nuc
    type(matrix), pointer :: satom_pt
    type(matrix) :: dens_0M, smat_M0
    logical :: lstat_S, lstat_D
    real(realk) :: tmp, tmp_sum
    type(lattice_domains_type) :: dom_s

    
    nbast = refcell%nBastReg
    natoms = refcell%natoms

    !Set up the domains
    call ltdom_init(dom_s, wconf%blat%dims, maxval(smat%cutoffs))
    call ltdom_getdomain(dom_s, smat%cutoffs, incl_redundant=.true.)

    call mat_init(dens_0M, nbast, nbast)
    call mat_init(smat_M0, nbast, nbast)
    
    loopm: do m = 1, dom_s%nelms
       indx_m = dom_s%indcs(m)

       call lts_copy(dens, -indx_m, lstat_D, dens_0M)
       if (.not.(lstat_D)) cycle
       call lts_copy(smat, indx_m, lstat_S, smat_M0)
       if (.not.(lstat_S)) cycle
       !dens_ptr => lts_get(dens, -indx_m)
       !if (.not. associated(dens_ptr)) cycle
       !smat_ptr => lts_get(smat, indx_m)
       !if (.not. associated(smat_ptr)) cycle
       
       call lts_mat_init(satom, indx_m)
       satom_pt => lts_get(satom, indx_m)
       
       muc = 1
       loopa: do a = 1, natoms
          nuc = 1
          loopb: do b = 1, natoms
             
             tmp = 0.0_realk
             loopmu: do mu = muc, muc + refcell%atom(a)%nContOrbREG - 1
                loopnu: do nu = nuc, nuc + refcell%atom(b)%nContOrbREG - 1
                   dens_indx = mu + (nu - 1)*nbast
                   smat_indx = nu + (mu - 1)*nbast
                   tmp = tmp + 2.0_realk * dens_0M%elms(dens_indx) * smat_M0%elms(smat_indx)
                enddo loopnu
             enddo loopmu
             nuc = nuc + refcell%atom(b)%nContOrbREG
            
             satom_pt%elms(a + (b - 1)*natoms) = tmp

          enddo loopb
          muc = muc + refcell%atom(a)%nContOrbREG
       enddo loopa
    enddo loopm

    if (wconf%debug_level >= 0) then
       tmp_sum = 0.0_realk
       do m=1, dom_s%nelms
          indx_m = dom_s%indcs(m)
          satom_pt => lts_get(satom, indx_m)
          do i = 1, natoms*natoms
             tmp_sum = tmp_sum + satom_pt%elms(i)
          enddo
       enddo
       if (abs(tmp_sum - refcell%nelectrons) > 1.0e-8_realk) then
          write(luerr, *) 'Atomic Overlap in Far-field does not integrate to necc',&
               abs(tmp_sum - refcell%nelectrons)
       endif
    endif

    
    if (wconf%debug_level > 6) then
       write(luerr,*) ''
       write(luerr,*) 'Atomic overlap'
       call lts_print(satom,luerr,1)
    endif

    call mat_free(dens_0M)
    call mat_free(smat_M0)
    
    call ltdom_free(dom_s)
    
  end subroutine get_atomic_overlap

  !> @brief Compute the Euclidean norm of a vector
  !> @author Elisa Rebolini
  !> @date November 2016
  !> @param[in] array Vector of real(realk) numbers
  !> @param[in] dim Dimension of the array
  function dnorm2(array,dim)
    real(realk),intent(in) :: array(:)
    integer,intent(in)     :: dim

    real(realk) :: dnorm2,tmp
    integer :: i

    tmp = 0.0_realk
    do i = 1, dim
       tmp = tmp + array(i)**2
    enddo
    dnorm2 = sqrt(tmp) 
  end function dnorm2
end module wannier_asymptotic_expansion

