
!> @file
!> \brief
!> \author L.Wirz
!> \date 2016

MODULE pari_auxbase_mod
 use precision
 use memory_handling
 use AO_typetype
 use basis_typetype
 use basis_type
 use molecule_typetype
 use molecule_type
 use readmolefile, only: build_distinct_atoms
 use typedeftype
 use lsmatrix_type
 use matrix_module
 use matrix_operations, only: mat_init,mat_print
 use matrix_util
 use typedef, only: typedef_init_setting, typedef_free_setting, typedef_set_default_setting, retrieve_output
 use integralInterfaceMOD
 use lsparameters
 use ls_integral_interface
 use integraloutput_type
 use lstiming
 use buildbasisset


 private
 public :: generate_AuxBasis_NRPARI

CONTAINS

!> \brief
!> \author lukas wirz
!> \date 2016
!> \param ls -- the whole LS object
subroutine generate_AuxBasis_NRPARI(lupri,luerr, ls)
  implicit none
  integer,intent(in)         :: lupri,luerr
  type(lsitem),intent(inout) :: ls

  type(basissetinfo),target  :: aux_basisset
  type(moleculeinfo),target  :: atomicmolecule
  type(atomtypeitem)         :: int_bs, ext_bs
  real(realk)                :: tc, tw, tc_tot, tw_tot, exp_val
  integer                    :: natomtypes, AT, i, j, n, rank, cutoff_scheme, cutoff, iprint, thistype, augm_scheme
  integer, pointer           :: natom(:), uatomtype(:), piv(:), internal_function_indices(:)
  logical                    :: spherical, contract, clebschgordon

  write(*,*) '*******************************************************************************'
  write(*,*) 'This routine probably works, but is to be treated as experimental.'
  write(*,*) 'Proceed at your own risk. You may have to increase the values of'
  write(*,*) 'maxAOangmom in lsutil/aotype and maxBASISsegment in lsutil/basisinfotype. (lnw)'
  write(*,*) '*******************************************************************************'
  write(lupri,*) '*******************************************************************************'
  write(lupri,*) 'This routine probably works, but is to be treated as experimental.'
  write(lupri,*) 'Proceed at your own risk. You may have to increase the values of'
  write(lupri,*) 'maxAOangmom in lsutil/aotype and maxBASISsegment in lsutil/basisinfotype. (lnw)'
  write(lupri,*) '*******************************************************************************'
  write(luerr,*) '*******************************************************************************'
  write(luerr,*) 'This routine probably works, but is to be treated as experimental.'
  write(luerr,*) 'Proceed at your own risk. You may have to increase the values of'
  write(luerr,*) 'maxAOangmom in lsutil/aotype and maxBASISsegment in lsutil/basisinfotype. (lnw)'
  write(luerr,*) '*******************************************************************************'

  call lstimer('START ',tc_tot,tw_tot,lupri)

  ! create basissettype object
  nAtomTypes = ls%input%basis%binfo(RegBasParam)%natomtypes
  !write(*,*) 'natomtypes',natomtypes
  call nullifybasisset(aux_basisset)
  call init_basissetinfo_types(aux_basisset,nAtomTypes)
  aux_basisset%spherical = ls%setting%SCHEME%NRPARI_SPHERICAL

  ! local variables
  spherical = ls%setting%SCHEME%NRPARI_SPHERICAL
  clebschgordon = ls%setting%SCHEME%NRPARI_CLEBSCHGORDON
  contract = ls%setting%SCHEME%NRPARI_CONTR
  !write(*,*)'chosen solid harmonic orbitals? ', spherical
  !write(*,*)'chosen CG with the solid harmonic orbitals? ', clebschgordon
  !write(*,*)'chosen contracted orbitals? ', contract
  cutoff_scheme = ls%setting%SCHEME%NRPARI_CUTOFF_SCHEME
  augm_scheme = ls%setting%SCHEME%NRPARI_AUGM_SCHEME
  exp_val = ls%setting%SCHEME%NRPARI_EXP
  !write(*,*)'augmentation scheme, exp',augm_scheme, exp_val

  !write(*,*) 'NCI',ls%input%basis%binfo(RegBasParam)%nchargeindex
  !write(*,*) 'LI',ls%input%basis%binfo(RegBasParam)%labelindex

  call mem_alloc(natom,natomtypes)
  call mem_alloc(uatomtype,natomtypes)
  IPRINT=ls%input%dalton%BASPRINT
  !write(*,*)'natomtypes',natomtypes
  !write(*,*)'natoms',ls%input%molecule%natoms
  call build_distinct_atoms(lupri, natomtypes, ls%input%molecule%natoms, natom, uatomtype, ls, iprint)
  !write(*,*)'natom',natom
  !write(*,*)'uatomtype',uatomtype

!  do i=1,natomtypes
!    write(*,*) ls%input%molecule%atom(natom(i))%charge ! correct order
!    write(*,*) ls%input%basis%binfo(RegBasParam)%atomtype(uatomtype(i))%charge ! weird order, fixed through uatomtype
!  enddo


! --------------------------------------------------------------------------------
! --------------------------------------------------------------------------------
  ! loop over atomtypes
  !write(*,*) 'looping over ',natomtypes, ' atomtypes.'
  do AT=1, nAtomTypes

    call lstimer('START ',tc,tw,lupri)

    !write(*,*)'cutoff scheme', cutoff_scheme
    if(cutoff_scheme .ge. 0) then
      cutoff = 2*(ls%input%basis%binfo(RegBasParam)%atomtype(uatomtype(AT))%nangmom-1) + cutoff_scheme
    else
      cutoff = -1
    endif
    !write(*,*)'cutoff', cutoff

    if(.not. spherical) then
      call cartesianise(lupri, luerr, ls%input%basis%binfo(RegBasParam)%atomtype(uatomtype(AT)))
    endif

    call copy_atomtypeitem(ls%input%basis%binfo(RegBasParam)%atomtype(uatomtype(AT)), int_bs)
    call copy_atomtypeitem(ls%input%basis%binfo(RegBasParam)%atomtype(uatomtype(AT)), ext_bs)

    if(.not. contract) then
      call decontract(lupri, luerr, int_bs)
      call decontract(lupri, luerr, ext_bs)
    endif

!    call print_abs(ext_bs)
!    call augment(lupri,luerr,ext_bs,augm_scheme,exp_val,spherical)
!    call print_abs(ext_bs)

    ! multiply non-augmented (for the two stage cholesky)
    call multiply(lupri, luerr, int_bs, spherical, clebschgordon)
    ! multiply exp
    call multiply(lupri, luerr, ext_bs, spherical, clebschgordon)
!    call print_abs(ext_bs)

!    call print_abs(ext_bs)
    call augment(lupri,luerr,ext_bs,augm_scheme,exp_val,spherical)
    ! call print_abs(ext_bs)

    call lstimer('product basis ',tc,tw,lupri)

    ! the internal product need never be truncated
!    call truncate(lupri,luerr,ext_bs,cutoff,spherical)
!    call print_abs(int_bs)
!    call print_abs(ext_bs)

    call lstimer('truncate',tc,tw,lupri)

    ! get a list of indices of II product functions (with multiplicity) relative to the list of
    ! {II, IX, XX} product functions, but sorted by angular momentum
    call get_internal_indices(lupri,luerr,int_bs,ext_bs,internal_function_indices,spherical)
    call free_atomtypeitem(aux_basisset%atomtype(uatomtype(AT)))
    call copy_atomtypeitem(ext_bs, aux_basisset%atomtype(uatomtype(AT)))
!    call print_abs(ext_bs)
    call free_atomtypeitem(ext_bs)

    call lstimer('indices',tc,tw,lupri)

    call build_atomicmolecule(ls%input%molecule,atomicmolecule,natom(AT),lupri)
    call get_pivots(lupri,luerr,ls%input,aux_basisset,atomicmolecule,AT,uatomtype,piv,rank, &
                                                                                 internal_function_indices,int_bs%totnorb,spherical)
    call free_atomtypeitem(int_bs)
    call mem_dealloc(internal_function_indices)
    call free_moleculeinfo(atomicmolecule)

    call lstimer('pivots',tc,tw,lupri)

    call basis_from_pivots(lupri, luerr, aux_basisset%atomtype(uatomtype(AT)), piv, rank, spherical)
    call mem_dealloc(piv)

    call lstimer('basis',tc,tw,lupri)

    !call print_abs(aux_basisset%atomtype(uatomtype(AT)))

    ! write to molecule
    thistype = LS%INPUT%MOLECULE%ATOM(natom(AT))%IDtype(1)
    do I=1,ls%input%molecule%natoms
      if(thistype .eq. LS%INPUT%MOLECULE%ATOM(I)%IDtype(1)) then
        LS%INPUT%MOLECULE%ATOM(I)%nContOrbNRPARIAUX = aux_basisset%atomtype(uatomtype(AT))%totnorb
        LS%INPUT%MOLECULE%ATOM(I)%nPrimOrbNRPARIAUX = aux_basisset%atomtype(uatomtype(AT))%totnprim
      endif
    enddo

    if((.not. spherical) .and. (aux_basisset%atomtype(uatomtype(AT))%nangmom.ge.9)) then
      write(lupri,*)'THIS MAY NOT GO AS INTENDED.  I PREDICT A FAILURE WHEN CALCULATING PARI COEFFICIENTS.'
      write(luerr,*)'THIS MAY NOT GO AS INTENDED.  I PREDICT A FAILURE WHEN CALCULATING PARI COEFFICIENTS.'
      ! and the reason is that cartesian functions with high m have very high norms which leads to (alpha|beta) with huge conditioning numbers
    endif

  enddo ! atomtypes

  aux_basisset%labelindex = 1
  aux_basisset%nbast = 0
  do AT=1, aux_basisset%natomtypes
    n=0
    thistype = LS%INPUT%MOLECULE%ATOM(natom(AT))%IDtype(1)
    do I=1,LS%INPUT%MOLECULE%nATOMs
      if(thistype .eq. LS%INPUT%MOLECULE%ATOM(I)%IDtype(1)) n=n+1
    enddo
    aux_basisset%nbast = aux_basisset%nbast + aux_basisset%atomtype(uatomtype(AT))%totnorb * n
    aux_basisset%nprimbast = aux_basisset%nprimbast + aux_basisset%atomtype(uatomtype(AT))%totnprim * n
  enddo
  aux_basisset%label = 'NRPARI   '


  call copy_basissetinfo(aux_basisset, ls%input%basis%binfo(NRPARIBasParam))
  ls%input%basis%wbasis(NRPARIBasParam) = .true.
  ls%input%molecule%nBastNRPARIAUX = aux_basisset%nbast
  ls%input%molecule%nprimBastNRPARIAUX = aux_basisset%nprimbast

  call free_basissetinfo(aux_basisset)
  call mem_dealloc(natom)
  call mem_dealloc(uatomtype)

  write(*,*) 'non-robust pari aux basis created'
  call lstimer('NR-PARI basis',tc_tot,tw_tot,lupri)
end subroutine generate_AuxBasis_NRPARI



!> \brief
!> \author lukas wirz
!> \date 2016
!> \param a_bs -- a basis set which is augmented and returned
!> \param augm_scheme -- specifies the augmentation
!> \param exp_val -- a reference exponent
!> \param spherical -- are the angular functions spherical harmonics or cartesians?
subroutine augment(lupri,luerr,a_bs,augm_scheme,exp_val,spherical)
  implicit none
  integer,intent(in)               :: lupri,luerr,augm_scheme
  type(atomtypeitem),intent(inout) :: a_bs
  real(realk),intent(in)           :: exp_val
  logical,intent(in)               :: spherical

  integer :: l,lmin,lmax,n_per_shell,SEG,n_ang,target_l,scheme_bak

  ! augmentation schemes:
  ! 0: none
  ! 1: 0 .. l+1
  ! 2: 0 .. l+1, l+2
  ! 3: 0 .. l+1, l+2, l+3
  ! 4: l+1
  ! 5: l+1, l+2
  ! 6: l+1, l+2, l+3
  ! 7: l+1, l+1(8e)
  ! 8: l+1, l+1(8e), l+2, l+2(8e)
  ! 9: l+1, l+1(8e), l+2, l+2(8e), l+3, l+3(8e)
  ! 10: l+1, l+1(8e), l+1(64e)
  ! 11: l+1, l+1(8e), l+1(64e), l+2, l+2(8e), l+2(64e)
  ! 12: l+1, l+1(8e), l+1(64e), l+2, l+2(8e), l+2(64e), l+3, l+3(8e), l+3(64e)

  !write(*,*)'>>>>>>>>>>>> augment >>>>>>>>>>>>'

  !if (augm_scheme.gt.10 .or. augm_scheme.lt.0) write(luerr,*)'SOMETHING WRONG WITH THE AUGMENTATION SCHEME.'

  target_l = 0
  scheme_bak = augm_scheme
  if (augm_scheme.gt.50) then ! lets choose a single function relative to 100
    target_l = augm_scheme - 100
    scheme_bak = -1
  endif

  select case(scheme_bak)
    case(-1)
      if(target_l.ge.0) then
        a_bs%nangmom = a_bs%nangmom + target_l
        lmin = a_bs%nangmom - 1
        lmax = a_bs%nangmom - 1
        n_per_shell = 1
      else
        a_bs%nangmom = a_bs%nangmom
        lmin = a_bs%nangmom - 1 + target_l
        lmax = a_bs%nangmom - 1 + target_l
        n_per_shell = 1
      endif
    case(0)
 !     write(*,*)'---- not augmenting ----'
      return
    case(1)
      a_bs%nangmom = a_bs%nangmom + 1
      lmin = 0
      lmax = a_bs%nangmom - 1
      n_per_shell = 1
    case(2)
      a_bs%nangmom = a_bs%nangmom + 2
      lmin = 0
      lmax = a_bs%nangmom - 1
      n_per_shell = 1
    case(3)
      a_bs%nangmom = a_bs%nangmom + 3
      lmin = 0
      lmax = a_bs%nangmom - 1
      n_per_shell = 1
    case(4)
      a_bs%nangmom = a_bs%nangmom + 1
      lmin = a_bs%nangmom - 1
      lmax = a_bs%nangmom - 1
      n_per_shell = 1
    case(5)
      a_bs%nangmom = a_bs%nangmom + 2
      lmin = a_bs%nangmom - 2
      lmax = a_bs%nangmom - 1
      n_per_shell = 1
    case(6)
      a_bs%nangmom = a_bs%nangmom + 3
      lmin = a_bs%nangmom - 3
      lmax = a_bs%nangmom - 1
      n_per_shell = 1
    case(7)
      a_bs%nangmom = a_bs%nangmom + 1
      lmin = a_bs%nangmom - 1
      lmax = a_bs%nangmom - 1
      n_per_shell = 2
    case(8)
      a_bs%nangmom = a_bs%nangmom + 2
      lmin = a_bs%nangmom - 2
      lmax = a_bs%nangmom - 1
      n_per_shell = 2
    case(9)
      a_bs%nangmom = a_bs%nangmom + 3
      lmin = a_bs%nangmom - 3
      lmax = a_bs%nangmom - 1
      n_per_shell = 2
    case(10)
      a_bs%nangmom = a_bs%nangmom + 1
      lmin = a_bs%nangmom - 1
      lmax = a_bs%nangmom - 1
      n_per_shell = 3
    case(11)
      a_bs%nangmom = a_bs%nangmom + 2
      lmin = a_bs%nangmom - 2
      lmax = a_bs%nangmom - 1
      n_per_shell = 3
    case(12)
      a_bs%nangmom = a_bs%nangmom + 3
      lmin = a_bs%nangmom - 3
      lmax = a_bs%nangmom - 1
      n_per_shell = 3
  end select

  do l=lmin,lmax
    if(spherical) then
      n_ang = 2*l+1
    else ! Cartesian
      n_ang = ((l+1)*(l+2))/2
    endif
    a_bs%totnorb = a_bs%totnorb + n_ang * n_per_shell
    a_bs%totnprim = a_bs%totnprim + n_ang * n_per_shell
    a_bs%shell(l+1)%norb = a_bs%shell(l+1)%norb + n_per_shell
    a_bs%shell(l+1)%nprim = a_bs%shell(l+1)%nprim + n_per_shell
    a_bs%shell(l+1)%nsegments = a_bs%shell(l+1)%nsegments + n_per_shell

    do SEG=a_bs%shell(l+1)%nsegments-n_per_shell+1, a_bs%shell(l+1)%nsegments
      a_bs%shell(l+1)%segment(SEG)%nrow = 1
      a_bs%shell(l+1)%segment(SEG)%ncol = 1
      call mem_alloc(a_bs%shell(l+1)%segment(SEG)%elms,1)
      call mem_alloc(a_bs%shell(l+1)%segment(SEG)%UCCelms,1)
      call mem_alloc(a_bs%shell(l+1)%segment(SEG)%exponents,1)
      a_bs%shell(l+1)%segment(SEG)%elms(1) = 1.0 ! normalised later
      a_bs%shell(l+1)%segment(SEG)%UCCelms(1) = 1.0 ! normalised later
      a_bs%shell(l+1)%segment(SEG)%exponents(1) = exp_val * 8.0**(SEG - 1) ! 1, 8, 64 ...
    enddo
  enddo

  !write(*,*)'<<<<<<<<<<<< augment <<<<<<<<<<<<'
end subroutine augment


!> \brief set the angular functions to Cartesian (irrespective of the initial state)
!> \author lukas wirz
!> \date 2016
!> \param a_bs -- a basis set which is modified and returned
subroutine cartesianise(lupri,luerr,a_bs)
  implicit none
  integer,intent(in)                :: lupri,luerr
  type(atomtypeitem),intent(inout)  :: a_bs

  integer                           :: l, totnprim, totnorb

  totnprim = 0
  totnorb = 0
  do l=0, a_bs%nangmom
    totnprim = totnprim + a_bs%shell(l+1)%nprim * (l+1)*(l+2)/2
    totnorb = totnorb + a_bs%shell(l+1)%norb * (l+1)*(l+2)/2
  enddo
  a_bs%totnprim = totnprim
  a_bs%totnorb = totnorb
end subroutine cartesianise


!> \brief decontract
!> \author lukas wirz
!> \date 2016
!> \param a_bs -- a basis set which is modified and returned
subroutine decontract(lupri,luerr,a_bs)
  implicit none
  integer,intent(in)                :: lupri,luerr
  type(atomtypeitem),intent(inout)  :: a_bs

  type(atomtypeitem)                :: tmp_a_bs
  integer                           :: l, seg_c, p, ncol, nprim, seg_u

!write(*,*) '>>>>>>>>>>> decontract >>>>>>>>>>>>>>'

  tmp_a_bs%family = .false.
  tmp_a_bs%nangmom = a_bs%nangmom
  tmp_a_bs%totnorb = a_bs%totnprim ! really
  tmp_a_bs%totnprim = a_bs%totnprim
  tmp_a_bs%charge = a_bs%charge
  tmp_a_bs%name = a_bs%name

  do l=0,a_bs%nangmom-1
    ! every primitive gets its own segment
    tmp_a_bs%shell(l+1)%norb = a_bs%shell(l+1)%nprim ! really
    tmp_a_bs%shell(l+1)%nprim = a_bs%shell(l+1)%nprim
    tmp_a_bs%shell(l+1)%nsegments = a_bs%shell(l+1)%nprim ! really
    ! allocate new segments
    seg_u = 1
    do seg_c=1, a_bs%shell(l+1)%nsegments
      do p=1, a_bs%shell(l+1)%segment(seg_c)%nrow
        tmp_a_bs%shell(l+1)%segment(seg_u)%nrow = 1
        tmp_a_bs%shell(l+1)%segment(seg_u)%ncol = 1
        call mem_alloc(tmp_a_bs%shell(l+1)%segment(seg_u)%elms,1)
        call mem_alloc(tmp_a_bs%shell(l+1)%segment(seg_u)%uccelms,1)
        call mem_alloc(tmp_a_bs%shell(l+1)%segment(seg_u)%exponents,1)
        seg_u = seg_u + 1
      enddo
    enddo
    ! gather data
    seg_u=1
    do seg_c=1, a_bs%shell(l+1)%nsegments
      ncol = a_bs%shell(l+1)%segment(seg_c)%ncol
      nprim = a_bs%shell(l+1)%segment(seg_c)%nrow
      do p=1,nprim
        ! really, it doesn't matter which elm and uccelm is copied as they are normalised, hence discarded, later
        tmp_a_bs%shell(l+1)%segment(seg_u)%elms(1) = 1.e0_realk
        tmp_a_bs%shell(l+1)%segment(seg_u)%uccelms(1) = 1.e0_realk
        tmp_a_bs%shell(l+1)%segment(seg_u)%exponents(1) = a_bs%shell(l+1)%segment(seg_c)%exponents(p)
        seg_u=seg_u + 1
      enddo
    enddo
  enddo

  call free_atomtypeitem(a_bs)
  call copy_atomtypeitem(tmp_a_bs, a_bs)
  call free_atomtypeitem(tmp_a_bs)

! write(*,*)'<<<<<<<<<<<< decontract <<<<<<<<<<<<'

end subroutine decontract


!> \brief return the internal product basis of the input basis
!> \author lukas wirz
!> \date 2016
!> \param a_bs -- a basis set which is modified and returned
!> \param spherical -- spherical harmonics or cartesians
!> \param clebschgordon -- for each combination, add functions from lmax - lmin .. lmax + lmin (only if spherical)
subroutine multiply(lupri,luerr,a_bs,spherical,clebschgordon)
  implicit none
  integer,intent(in)               :: lupri,luerr
  type(atomtypeitem),intent(inout) :: a_bs
  logical, intent(in)              :: spherical, clebschgordon

  type(atomtypeitem) :: tmp_a_bs
  integer            :: l1, l2, l_new, s1, s2, s_new, r1, r2, r_new, c1, c2, c_new, index1, index2, index_new
  integer            :: n_prim1, n_prim2, n_prim, n_cont1, n_cont2, n_cont
  integer            :: s2_first, r2_first, c2_first, l_first, l_last, n_ang, c_fac

  ! solid harmonics --> clebsch gordon lmax - lmin .. lmax + lmin
  ! Cartesian --> l1 + l2

!write(*,*) '>>>>>>>>>>>> multiply >>>>>>>>>>>>'
  if (2*a_bs%nangmom - 1 .gt. maxaoangmom) call lsquit('maxAOangmom is too small', lupri)

  ! setup tmp_a_bs in general ... zero things etc
  tmp_a_bs%nangmom = 2*a_bs%nangmom - 1
  tmp_a_bs%family = .false.
  tmp_a_bs%charge = a_bs%charge
  tmp_a_bs%totnorb = 0
  tmp_a_bs%totnprim = 0
  tmp_a_bs%name = a_bs%name
  do l_new=0, tmp_a_bs%nangmom-1
    tmp_a_bs%shell(l_new+1)%nprim = 0
    tmp_a_bs%shell(l_new+1)%norb = 0
    tmp_a_bs%shell(l_new+1)%nsegments = 0
  enddo

  do l1=0,a_bs%nangmom-1
    do l2=l1,a_bs%nangmom-1
      if(spherical .and. clebschgordon) then
        l_first = max(l1,l2) - min(l1,l2)
        l_last = max(l1,l2) + min(l1,l2)
      else ! Cartesian or spherical without CG
        l_first = l1 + l2
        l_last = l_first
      endif
      do l_new=l_first,l_last
        s_new = tmp_a_bs%shell(l_new+1)%nsegments ! initally zero, counting up
        do s1=1,a_bs%shell(l1+1)%nsegments
          s2_first = 1
          if(l1.eq.l2) s2_first = s1 ! no duplicate segments
          do s2=s2_first,a_bs%shell(l2+1)%nsegments
            ! get dimensions
            n_prim1 = a_bs%shell(l1+1)%segment(s1)%nrow
            n_prim2 = a_bs%shell(l2+1)%segment(s2)%nrow
            n_prim = n_prim1 * n_prim2
            n_cont1 = a_bs%shell(l1+1)%segment(s1)%ncol
            n_cont2 = a_bs%shell(l2+1)%segment(s2)%ncol
            n_cont = n_cont1 * n_cont2
            if(l1.eq.l2 .and. s1.eq.s2) then ! no duplicate orbitals
              !n_prim = ((n_prim1+1)*n_prim1)/2 ! n_prim1=n_prim2
              n_cont = ((n_cont1+1)*n_cont1)/2 ! n_cont1=n_cont2
            endif
            ! update shell content
            tmp_a_bs%shell(l_new+1)%nsegments = tmp_a_bs%shell(l_new+1)%nsegments + 1
            s_new = tmp_a_bs%shell(l_new+1)%nsegments ! local variable
            tmp_a_bs%shell(l_new+1)%nprim = tmp_a_bs%shell(l_new+1)%nprim + n_prim
            tmp_a_bs%shell(l_new+1)%norb = tmp_a_bs%shell(l_new+1)%norb + n_cont
            ! write segment content
            tmp_a_bs%shell(l_new+1)%segment(s_new)%nrow = n_prim
            tmp_a_bs%shell(l_new+1)%segment(s_new)%ncol = n_cont
            call mem_alloc(tmp_a_bs%shell(l_new+1)%segment(s_new)%elms, n_prim*n_cont)
            call mem_alloc(tmp_a_bs%shell(l_new+1)%segment(s_new)%uccelms, n_prim*n_cont)
            call mem_alloc(tmp_a_bs%shell(l_new+1)%segment(s_new)%exponents, n_prim)

            r_new = 0
            do r1=1,n_prim1
              r2_first = 1
              do r2=r2_first,n_prim2
                r_new = r_new+1
                tmp_a_bs%shell(l_new+1)%segment(s_new)%exponents(r_new) = &
                                             a_bs%shell(l1+1)%segment(s1)%exponents(r1) + a_bs%shell(l2+1)%segment(s2)%exponents(r2)
                c_new = 0
                do c1=1,n_cont1
                  c2_first = 1
                  if(l1.eq.l2 .and. s1.eq.s2) c2_first = c1 ! no duplicate orbitals
                  do c2=c2_first,n_cont2
                    if ((l1.eq.l2 .and. s1.eq.s2) .and. (c1.ne.c2)) then
                      c_fac = 2
                    else
                      c_fac = 1
                    endif
                    c_new = c_new + 1
                    index1 = n_prim1*(c1-1) + r1
                    index2 = n_prim2*(c2-1) + r2
                    index_new = n_prim*(c_new-1) + r_new
                    tmp_a_bs%shell(l_new+1)%segment(s_new)%elms(index_new) = c_fac * &
                                               a_bs%shell(l1+1)%segment(s1)%elms(index1) * a_bs%shell(l2+1)%segment(s2)%elms(index2)
                    tmp_a_bs%shell(l_new+1)%segment(s_new)%uccelms(index_new) = c_fac * &
                                         a_bs%shell(l1+1)%segment(s1)%uccelms(index1) * a_bs%shell(l2+1)%segment(s2)%uccelms(index2)
                  enddo
                enddo
              enddo
            enddo
            call normalize_orbitals_wrapper(lupri, l_new, r_new, c_new, tmp_a_bs%shell(l_new+1)%segment(s_new)%exponents, &
                                                                                        tmp_a_bs%shell(l_new+1)%segment(s_new)%elms)
          enddo ! s2
        enddo ! s1
      enddo ! l new
    enddo ! l2
  enddo ! l1

  do l_new=0,tmp_a_bs%nangmom-1
    if (spherical) then
      n_ang = 2*l_new + 1
    else ! Cartesian
      n_ang = ((l_new+1)*(l_new+2))/2
    endif
    tmp_a_bs%totnprim =  tmp_a_bs%totnprim + tmp_a_bs%shell(l_new+1)%nprim * n_ang
    tmp_a_bs%totnorb =  tmp_a_bs%totnorb + tmp_a_bs%shell(l_new+1)%norb * n_ang
  enddo

  call free_atomtypeitem(a_bs)
  call copy_atomtypeitem(tmp_a_bs,a_bs)
  call free_atomtypeitem(tmp_a_bs)

!write(*,*) '<<<<<<<<<<<< multiply <<<<<<<<<<<<'

end subroutine multiply


!> \brief remove all functions with l > l_max
!> \author lukas wirz
!> \date 2016
!> \param a_bs -- a basis set which is modified and returned
!> \param spherical -- spherical harmonics or cartesians
!> \param l_max -- maximum preserved l
subroutine truncate(lupri,luerr,a_bs,l_max,spherical)
  implicit none
  integer,intent(in)               :: lupri,luerr,l_max
  type(atomtypeitem),intent(inout) :: a_bs
  logical,intent(in)               :: spherical

  integer            :: l, seg, n_ang

!  write(*,*)'<<<<<<<<<<<< truncate <<<<<<<<<<<<'

  ! we chose not to truncate
  if (l_max .eq. -1) return

  ! nothing to truncate
  if (l_max .gt. a_bs%nangmom-1) return

!  write(*,*)'removing all l=',l_max+1,'to',a_bs%nangmom-1

  ! remove all functions above l_max
  do l=l_max+1,a_bs%nangmom-1
    do seg=1,a_bs%shell(l+1)%nsegments
      a_bs%shell(l+1)%segment(seg)%nrow = 0
      a_bs%shell(l+1)%segment(seg)%ncol = 0
      call mem_dealloc(a_bs%shell(l+1)%segment(seg)%elms)
      call mem_dealloc(a_bs%shell(l+1)%segment(seg)%uccelms)
      call mem_dealloc(a_bs%shell(l+1)%segment(seg)%exponents)
    enddo
    if (spherical) then
      n_ang = 2*l + 1
    else ! Cartesian
      n_ang = ((l+1)*(l+2))/2
    endif
    a_bs%totnprim = a_bs%totnprim - a_bs%shell(l+1)%nprim * n_ang
    a_bs%totnorb = a_bs%totnorb - a_bs%shell(l+1)%norb * n_ang
    a_bs%shell(l+1)%nsegments = 0
    a_bs%shell(l+1)%nprim = 0
    a_bs%shell(l+1)%norb = 0
  enddo
  a_bs%nangmom = l_max + 1

!  write(*,*)'>>>>>>>>>>>> truncate >>>>>>>>>>>>'

end subroutine truncate


!> \brief build integral matrix of basisset, two-stage Cholesky decompose in order to find the pivots
!> \author lukas wirz
!> \date 2016
!> \param input --
!> \param aux_basisset -- the basis set
!> \param atomicmolecule --
!> \param atom_type --
!> \param uatomtype --
!> \param piv -- vector of pivots to be returned
!> \param rank -- rank as returned from cholesky
!> \param indices -- indices of internal functions
!> \param n_funcs_internal -- number of internal functions, rest is external
!> \param spherical -- spherical or cartesian
subroutine get_pivots(lupri,luerr,input,aux_basisset,atomicmolecule,atom_type,uatomtype,piv,rank,indices,n_funcs_internal,spherical)
  implicit none
  type(daltoninput),intent(in)         :: input
  type(moleculeinfo),target,intent(in) :: atomicmolecule
  type(basissetinfo),target,intent(in) :: aux_basisset
  integer,intent(in)                   :: lupri, luerr, atom_type, n_funcs_internal
  integer,pointer,intent(in)           :: uatomtype(:), indices(:)
  logical,intent(in)                   :: spherical
  integer,pointer,intent(inout)        :: PIV(:)
  integer,intent(inout)                :: rank

  type(basisinfo),target :: tmp_basisinfo
  type(lssetting)        :: tmp_atomicSetting
  integer                :: iao, igrid,nbast, info
  real(realk),pointer    :: alphabeta(:,:), work(:)
  real(realk)            :: pchol_thr_i, pchol_thr_ii

  integer :: i

!  write(*,*)'>>>>>>>>>>>> get_pivots >>>>>>>>>>>>'

  call typedef_init_setting(tmp_atomicSetting)
  call typedef_set_default_setting(tmp_atomicSetting,input)
  !tmp_basisinfo%wbasis(1) = .false.
  !tmp_basisinfo%wbasis(2) = .false.
  !tmp_basisinfo%wbasis(3) = .false.
  !tmp_basisinfo%wbasis(4) = .false.
  !tmp_basisinfo%wbasis(5) = .false.
  !tmp_basisinfo%wbasis(6) = .false.
  !tmp_basisinfo%wbasis(7) = .false.
  tmp_basisinfo%wbasis(8) = .true.
  tmp_basisinfo%binfo(8) = aux_basisset
  do iAO=1,4
    tmp_atomicSetting%basis(iAO)%p => tmp_basisinfo
    tmp_atomicSetting%molecule(iAO)%p => atomicmolecule
    tmp_atomicSetting%fragment(iAO)%p => atomicmolecule
  enddo

  tmp_atomicSetting%scheme%hermiteecoeff = spherical
  tmp_atomicSetting%scheme%dospherical = spherical

  tmp_atomicSetting%scheme%admm_exchange = .FALSE.
  tmp_atomicSetting%scheme%densfit = .FALSE.
  tmp_atomicSetting%scheme%df_k = .FALSE.
  tmp_atomicSetting%scheme%PARI_J = .FALSE.
  tmp_atomicSetting%scheme%PARI_K = .FALSE.
  tmp_atomicSetting%scheme%MOPARI_K = .FALSE.
  tmp_atomicSetting%scheme%FMM = .FALSE.
  tmp_atomicSetting%scheme%MBIE_SCREEN = .FALSE.
  tmp_atomicSetting%scheme%QQR_SCREEN = .FALSE.
  tmp_atomicSetting%scheme%CS_SCREEN = .FALSE.
  tmp_atomicSetting%scheme%PS_SCREEN = .FALSE.
  tmp_atomicSetting%scheme%LINK = .FALSE.
  tmp_atomicSetting%scheme%DFT%CS00 = .FALSE.
  tmp_atomicSetting%scheme%DFT%LB94 = .FALSE.
  tmp_atomicSetting%scheme%DFT%griddone = 0 !the DFT grid should be made for all unique atoms
  igrid = tmp_atomicSetting%scheme%DFT%igrid
  tmp_atomicSetting%scheme%DFT%GridObject(igrid)%griddone = 0

  tmp_atomicsetting%basis(1)%p%binfo(8)%nchargeindex = 0
  tmp_atomicsetting%basis(1)%p%binfo(8)%labelindex = 1
  tmp_atomicsetting%basis(1)%p%binfo(8)%spherical  = spherical
  ! write(*,*) 'LI',ls%input%basis%binfo(RegBasParam)%labelindex

  !tmp_atomicSetting%scheme%NOFAMILY = .true.
  !tmp_atomicSetting%scheme%NOSEGMENT = .true.

  nbast = tmp_atomicsetting%basis(1)%p%binfo(8)%atomtype(uatomtype(atom_type))%totnorb

! alphabeta with coulomb metric
  call mem_alloc(alphabeta,nbast,nbast)
  call ls_dzero(alphabeta,nbast**2)
  call initIntegralOutputDims(tmp_atomicsetting%Output,nbast,1,nbast,1,1)
  call ls_getIntegrals(AOnrpariaux,AOempty,AOnrpariaux,AOempty,&
                                                        CoulombOperator,RegularSpec,ContractedInttype,tmp_atomicsetting,lupri,luerr)
  call retrieve_Output(lupri,tmp_atomicsetting,alphabeta,tmp_atomicsetting%IntegralTransformGC)

  pchol_thr_i = tmp_atomicSETTING%SCHEME%NRPARI_PCHOL_THRESHOLD1
  pchol_thr_ii = tmp_atomicSETTING%SCHEME%NRPARI_PCHOL_THRESHOLD2

  call mem_alloc(PIV,nbast)
  PIV = 0
  call mem_alloc(work,2*nbast)
  call ls_dzero(work,2*nbast)
! ( UPLO, N, A, LDA, PIV, RANK, TOL, WORK, INFO )
  ! call lev3pchol('U', nbast, alphabeta, nbast, PIV, rank, pchol_thr_i, work, info)
  call twostage_lev3pchol('U', nbast, alphabeta, nbast, PIV, rank, pchol_thr_i, pchol_thr_ii, indices, n_funcs_internal, work, info)

  call mem_dealloc(work)
  call mem_dealloc(alphabeta)
  call typedef_free_setting(tmp_atomicSETTING)

end subroutine get_pivots


!> \brief get a list of indices of II product functions (with multiplicity) relative to the list of
! {II, IX, XX} product functions, but sorted by angular momentum
!> \author lukas wirz
!> \date 2016
!> \param int_prod_bs -- internal product of regular basis set
!> \param ext_prod_bs -- internal product of augmented basis set
!> \param int_func_indices -- list of int_prod_bs functions in ext_prod_bs
!> \param spherical -- spherical or Cartesian?
subroutine get_internal_indices(lupri,luerr,int_prod_bs,ext_prod_bs,int_func_indices,spherical)
  implicit none
  integer,intent(in)            :: lupri,luerr
  type(atomtypeitem),intent(in) :: int_prod_bs, ext_prod_bs
  integer,pointer,intent(inout) :: int_func_indices(:)
  logical,intent(in)            :: spherical

  integer :: l, n_ang, s1, s2, c1, c2, p, i, n_prim1, n_prim2, ang, int_ptr, ext_ptr, ll, n_ang2


  call mem_alloc(int_func_indices, int_prod_bs%totnorb)
  int_func_indices = 0

  int_ptr = 0

  do l=0,int_prod_bs%nangmom - 1
    if (spherical) then
      n_ang = 2*l + 1
    else ! Cartesian
      n_ang = ((l+1)*(l+2))/2
    endif
    si: do s1=1, int_prod_bs%shell(l+1)%nsegments
      ci: do c1=1, int_prod_bs%shell(l+1)%segment(s1)%ncol
        ! loop though external orbitals,
        ext_ptr = 0
        if (l.gt.0) then
          do ll=0,l-1
            if (spherical) then
              n_ang2 = 2*ll + 1
            else ! Cartesian
              n_ang2 = ((ll+1)*(ll+2))/2
            endif
            ext_ptr = ext_ptr + ext_prod_bs%shell(ll+1)%norb * n_ang2
          enddo
        endif
        ! write(*,*)'int/ext_ptr', int_ptr,ext_ptr
        ! search for entry in ext basis
        se: do s2=1, ext_prod_bs%shell(l+1)%nsegments
          ce: do c2=1, ext_prod_bs%shell(l+1)%segment(s2)%ncol
!            write(*,*) 'searching for ', int_prod_bs%shell(l+1)%segment(s1)%exponents(1), 'etc'
            ext_ptr = ext_ptr + n_ang
            n_prim1 = int_prod_bs%shell(l+1)%segment(s1)%nrow
            n_prim2 = ext_prod_bs%shell(l+1)%segment(s2)%nrow
            ! does the number of primitives match?
            if(n_prim1 .ne. n_prim2 ) cycle ce
            ! do their values match?
            do p=1,n_prim2
              if(abs(int_prod_bs%shell(l+1)%segment(s1)%exponents(p) - &
                                                            ext_prod_bs%shell(l+1)%segment(s2)%exponents(p)) .gt. 1.e-10_realk) then
                cycle ce
              endif
            enddo
            ! match !
            ! if match, check if index had been added to list previously
            do i=1,int_ptr
              if(int_func_indices(i) .eq. ext_ptr - n_ang + 1) then
!                write(*,*) ext_ptr - n_ang + 1, 'already at',i
                cycle ce ! has been referred to already
              endif
            enddo
            ! if not added previously, add to index list
            do ang=1,n_ang
              int_func_indices(int_ptr + ang) = ext_ptr - n_ang + ang
!              write(*,*) 'writing', ext_ptr , n_ang , ang , 'to', int_ptr, ang
            enddo
            ! all work done, go to next value
            int_ptr = int_ptr + n_ang
            cycle ci
          enddo ce
        enddo se
        int_ptr = int_ptr + n_ang
      enddo ci
    enddo si
  enddo

end subroutine get_internal_indices


!> \brief iterate through the existing basis and copy the entries \in piv to the new basis
!> \author lukas wirz
!> \date 2016
!> \param a_bs -- product basis, gets thinned out according to PIV
!> \param piv -- pivots as reported by cholesky
!> \param rank -- rank as reported by cholesky
!> \param spherical -- spherical or cartesian?
subroutine basis_from_pivots(lupri, luerr, a_bs, piv, rank, spherical)
  implicit none
  integer,intent(in)               :: lupri,luerr,rank
  type(atomtypeitem),intent(inout) :: a_bs
  integer,pointer,intent(in)       :: piv(:)
  logical,intent(in)               :: spherical

  type(atomtypeitem) :: tmp_a_bs
  integer :: l, n_ang, s,  i, orb_count, norb,nprim,s_curr,p,o_source,o_dest,n_orbitals_in_seg,indexx
  logical,pointer  :: keep(:)

!write(*,*) '>>>>>>>>>>>> base from piv >>>>>>>>>>>>'

  tmp_a_bs%family = .false.
  tmp_a_bs%nangmom = 0
  tmp_a_bs%totnprim = 0
  tmp_a_bs%totnorb = 0
  tmp_a_bs%charge = a_bs%charge
  tmp_a_bs%name = a_bs%name

  orb_count = 0
  do l=0,a_bs%nangmom - 1
!    write(*,*) 'starting l=', l
    tmp_a_bs%shell(l+1)%nprim = 0
    tmp_a_bs%shell(l+1)%norb = 0
    tmp_a_bs%shell(l+1)%nsegments = 0
    if (spherical) then
      n_ang = 2*l + 1
    else ! Cartesian
      n_ang = ((l+1)*(l+2))/2
    endif
    seg_loop: do s=1,a_bs%shell(l+1)%nsegments
      ! which orbitals from this segment do i keep?
      call mem_alloc(keep, a_bs%shell(l+1)%segment(s)%ncol)
      keep = .false.
      n_orbitals_in_seg = 0
      do i=1,rank
        if ((piv(i) .gt. orb_count) .and. (piv(i) .le. orb_count + a_bs%shell(l+1)%segment(s)%ncol * n_ang)) then
          indexx = CEILING((piv(i) - orb_count)/real(n_ang))
          if(keep(indexx) .eqv. .false.) n_orbitals_in_seg = n_orbitals_in_seg + 1
          keep(indexx) = .true.
        endif
      enddo
      if (n_orbitals_in_seg .eq. 0) then
        orb_count = orb_count + a_bs%shell(l+1)%segment(s)%ncol * n_ang
        call mem_dealloc(keep)
        cycle seg_loop ! bail out
      endif
      tmp_a_bs%nangmom = max(tmp_a_bs%nangmom,l+1)
      tmp_a_bs%totnprim = tmp_a_bs%totnprim + a_bs%shell(l+1)%segment(s)%nrow * n_ang
      tmp_a_bs%totnorb = tmp_a_bs%totnorb + n_orbitals_in_seg * n_ang
      tmp_a_bs%shell(l+1)%nprim = tmp_a_bs%shell(l+1)%nprim + a_bs%shell(l+1)%segment(s)%nrow
      tmp_a_bs%shell(l+1)%norb = tmp_a_bs%shell(l+1)%norb + n_orbitals_in_seg
      tmp_a_bs%shell(l+1)%nsegments = tmp_a_bs%shell(l+1)%nsegments + 1
      s_curr = tmp_a_bs%shell(l+1)%nsegments
      nprim = a_bs%shell(l+1)%segment(s)%nrow
      norb = n_orbitals_in_seg
      tmp_a_bs%shell(l+1)%segment(s_curr)%nrow = nprim
      tmp_a_bs%shell(l+1)%segment(s_curr)%ncol = norb
      call mem_alloc( tmp_a_bs%shell(l+1)%segment(s_curr)%elms, nprim*norb )
      call mem_alloc( tmp_a_bs%shell(l+1)%segment(s_curr)%uccelms, nprim*norb )
      call mem_alloc( tmp_a_bs%shell(l+1)%segment(s_curr)%exponents, nprim )
      o_source = 0
      do o_dest=1,norb
        o_source = o_source + 1
        do while (keep(o_source) .eqv. .false.)
          o_source = o_source + 1
        enddo
        do p=1,nprim
          tmp_a_bs%shell(l+1)%segment(s_curr)%elms(nprim*(o_dest-1)+p) = a_bs%shell(l+1)%segment(s)%elms(nprim*(o_source-1)+p)
          tmp_a_bs%shell(l+1)%segment(s_curr)%uccelms(nprim*(o_dest-1)+p) = a_bs%shell(l+1)%segment(s)%uccelms(nprim*(o_source-1)+p)
        enddo
      enddo
      do p=1,nprim
        tmp_a_bs%shell(l+1)%segment(s_curr)%exponents(p) = a_bs%shell(l+1)%segment(s)%exponents(p)
      enddo
      orb_count = orb_count + a_bs%shell(l+1)%segment(s)%ncol * n_ang

      call mem_dealloc(keep)
    enddo seg_loop
  enddo

  call free_atomtypeitem(a_bs)
  call copy_atomtypeitem(tmp_a_bs, a_bs)
  call free_atomtypeitem(tmp_a_bs)

! write(*,*) '<<<<<<<<<<<< base from piv <<<<<<<<<<<<'
end subroutine basis_from_pivots



subroutine print_abs(abs)
  implicit none
  type(atomtypeitem),intent(in) :: abs
  integer :: l, s

  write(*,*) '---general---'
  write(*,*) 'fam', abs%family
  write(*,*) 'nangmom', abs%nangmom
  write(*,*) 'totnprim', abs%totnprim
  write(*,*) 'totnorb', abs%totnorb
  write(*,*) 'charge', abs%charge
  write(*,*) 'name', abs%name

  do l=0,abs%nangmom-1
    write(*,*) '--- l=',l
    write(*,*) '  nrpim', abs%shell(l+1)%nprim
    write(*,*) '  norb', abs%shell(l+1)%norb
    write(*,*) '  nsegments', abs%shell(l+1)%nsegments
    do s=1,abs%shell(l+1)%nsegments
      write(*,*) '  --- s=',s
      write(*,*) '    nrow', abs%shell(l+1)%segment(s)%nrow
      write(*,*) '    ncol', abs%shell(l+1)%segment(s)%ncol
      write(*,*) '    elms', abs%shell(l+1)%segment(s)%elms
      write(*,*) '    uccelms', abs%shell(l+1)%segment(s)%uccelms
      write(*,*) '    exp', abs%shell(l+1)%segment(s)%exponents
    enddo
  enddo

  write(*,*) '---end---'

end subroutine print_abs


!> \brief
!> \author lukas wirz
!> \date 2016
subroutine normalize_orbitals_wrapper(lupri, angmom, nprim, norb, exponents, elms)
  implicit none
  integer, intent(in)               :: lupri, angmom, nprim, norb
  real(realk),pointer,intent(in)    :: exponents(:)
  real(realk),pointer,intent(inout) :: elms(:)

  type(lsmatrix):: Exp       !Vector of exponents
  type(lsmatrix):: CmatrixIN   !matrix of non-Normalized ContractionCoefficients
  type(lsmatrix):: CmatrixOUT  !OUTPUT: Normalised matrix of C Coefficients
  integer :: o,p

  exp%nrow = nprim
  exp%ncol = 1
  exp%elms => exponents
  cmatrixIN%nrow = nprim
  cmatrixIN%ncol = norb
  cmatrixIN%elms => elms
  cmatrixOUT%nrow = nprim
  cmatrixOUT%ncol = norb
  call mem_alloc(cmatrixOUT%elms,nprim*norb)

  call NORMALIZE_ORBITALS_mod(lupri, 11, angmom+1, norb, nprim, exp, CmatrixIN, CmatrixOUT)

  do o=1,norb
    do p=1,nprim
      elms(nprim*(o-1)+p) = cmatrixOUT%elms((nprim*(o-1)+p))
    enddo
  enddo
  call mem_dealloc(cmatrixOUT%elms)
end subroutine normalize_orbitals_wrapper


!> \brief Normalize the orbitals
!> \author T. Kjaergaard, mod: LNW
!> \date 2010, mod: 2016
SUBROUTINE NORMALIZE_ORBITALS_mod(LUPRI,IPRINT,angmom,nOrbitals,nExp,Exp,CmatrixIN,CmatrixOUT)
  implicit none
  TYPE(lsmatrix):: Exp       !Vector of exponents
  TYPE(lsmatrix),intent(in)    :: CmatrixIN   !matrix of non-Normalized ContractionCoefficients
  TYPE(lsmatrix),intent(inout) :: CmatrixOUT  !OUTPUT: Normalised matrix of C Coefficients
  INTEGER     :: nEXP      !number of exponents
  INTEGER     :: nOrbitals !number of contracted orbitals
  INTEGER     :: angmom    !angular momentum=angmom-1
  INTEGER     :: LUPRI,M,L,N,IPRINT
  real(realk) :: SUM,T,npi2
  real(realk) :: PI,PIPPI,D0,D1,D2,D4,DP25,DP5,DP75,THRMIN,EXPMIN

  PI=acos(-1.e0_realk)
  D0 = 0.0E0_realk
  D1 = 1.0E0_realk
  D2 = 2.0E0_realk
  D4 = 4.0E0_realk
  DP25 = 0.25E0_realk
  DP5 = 0.5E0_realk
  DP75 = 0.75E0_realk
  THRMIN = 1E-17_realk
  EXPMIN = 1E-10_realk
  PIPPI = (0.5E0_realk/PI)**(0.75E0_realk)

  IF (nExp.EQ. 1 .AND. ABS(Exp%elms(1)).LE.EXPMIN) THEN
    CmatrixOUT%elms(1) = D1
  ELSE
    DO N = 1, nOrbitals
      SUM = D0
      DO L = 1, nExp
        DO M = 1, nExp
          T = D2*SQRT(Exp%elms(L)*Exp%elms(M))/(Exp%elms(L)+Exp%elms(M))
          NPi2 = (D4*Exp%elms(L))**(DP5*angmom+DP25)*(D4*Exp%elms(m))**(DP5*angmom+DP25) * PIPPI*PIPPI
          SUM = SUM + CmatrixIN%elms(L+(N-1)*nExp)*CmatrixIN%elms(M+(N-1)*nExp)*(T**(angmom + DP5)) / npi2
        END DO
      END DO
      IF (SQRT(SUM) .LT. THRMIN) THEN
        WRITE (LUPRI,*) 'INPUT ERROR: CGTO no.',N,'of angular momentum',angmom,'has zero norm.'
        call lsquit('CGTO with zero norm.',lupri)
      ENDIF
      SUM=D1/SQRT(SUM)
      DO L=1,nExp
        CmatrixOUT%elms(L+(N-1)*nExp)=CmatrixIN%elms(L+(N-1)*nExp)*SUM
      END DO
    END DO
  END IF
END SUBROUTINE NORMALIZE_ORBITALS_mod


END MODULE pari_auxbase_mod
