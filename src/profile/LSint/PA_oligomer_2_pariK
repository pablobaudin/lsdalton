#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > PA_oligomer_2_pariK.info <<'%EOF%'
   PA_oligomer_2_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > PA_oligomer_2_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
EXCITATION DIAGNOSTIC
CAM-B3LYP/6-31G* GEOMETRY
Atomtypes=2  Nosymmetry
Charge=6.0 Atoms=10
C        1.2711424564            0.0435414958            0.0000000000
C       -1.2711424564           -0.0435414958            0.0000000000
C        2.7280850049            2.3528897904            0.0000000000
C       -2.7280850049           -2.3528897904            0.0000000000
C        5.2670477481            2.4445973490            0.0000000000
C       -5.2670477481           -2.4445973490            0.0000000000
C        6.7203282027            4.7714208495            0.0000000000
C       -6.7203282027           -4.7714208495            0.0000000000
C        9.2413877148            4.8721859010            0.0000000000
C       -9.2413877148           -4.8721859010            0.0000000000
Charge=1.0 Atoms=12
H        2.3300816888           -1.7225148286            0.0000000000
H       -2.3300816888            1.7225148286            0.0000000000
H        1.6669110420            4.1179387805            0.0000000000
H       -1.6669110420           -4.1179387805            0.0000000000
H        6.3361253939            0.6847800893            0.0000000000
H       -6.3361253939           -0.6847800893            0.0000000000
H        5.6384813113            6.5224829663            0.0000000000
H       -5.6384813113           -6.5224829663            0.0000000000
H       10.3824190817            3.1646086695            0.0000000000
H      -10.3824190817           -3.1646086695            0.0000000000
H       10.2502756743            6.6563014475            0.0000000000
H      -10.2502756743           -6.6563014475            0.0000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > PA_oligomer_2_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >PA_oligomer_2_pariK.check
cat >> PA_oligomer_2_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
